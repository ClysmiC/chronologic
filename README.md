# README #

ChronoLogic is a time-travel based puzzle/platformer prototype created in Monogame. Co-operate with previous iterations of yourself to make it to the end of each level! (Gameplay mechanics are similar to the time-travel elements of games like Braid and The Talos Principle).

# Check out a gameplay clip #
[![](https://img.youtube.com/vi/PQS8aeNv9go/0.jpg)](https://www.youtube.com/watch?v=PQS8aeNv9go)