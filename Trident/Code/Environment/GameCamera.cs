﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Environment
{
    public class GameCamera
    {
        //position of center of camera
        public Vector2 Position { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public GameCamera(int x, int y, int width, int height)
        {
            Position = new Vector2(x, y);
            Width = width;
            Height = height;
        }
    }
}
