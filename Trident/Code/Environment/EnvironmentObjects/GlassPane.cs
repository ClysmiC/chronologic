﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Utility;

namespace Trident.Code.Environment.EnvironmentObjects
{
    public class GlassPane
    {
        public bool IsBroken { get; set; }
        public bool IsHorizontal { get; set; }
        public Vector2[] HitboxCoordinates { get; private set; }

        public GlassOrientation Orientation { get; set; }

        public GlassPane(float x, float y, GlassOrientation orientation, float length)
        {
            IsBroken = false;
            Orientation = orientation;
            IsHorizontal = (Orientation == GlassOrientation.HBOT || Orientation == GlassOrientation.HMID || Orientation == GlassOrientation.HTOP);

            HitboxCoordinates = new Vector2[4];

            switch (orientation)
            {
                case GlassOrientation.VLEFT:
                    HitboxCoordinates[0] = new Vector2(x, y);
                    HitboxCoordinates[1] = new Vector2(x + 7, y);
                    HitboxCoordinates[2] = new Vector2(x + 7, y + length);
                    HitboxCoordinates[3] = new Vector2(x, y + length);
                    break;

                case GlassOrientation.VMID:
                    HitboxCoordinates[0] = new Vector2(x + 12, y);
                    HitboxCoordinates[1] = new Vector2(x + 12 + 7, y);
                    HitboxCoordinates[2] = new Vector2(x + 12 + 7, y + length);
                    HitboxCoordinates[3] = new Vector2(x + 12, y + length);
                    break;

                case GlassOrientation.VRIGHT:
                    HitboxCoordinates[0] = new Vector2(x + 24, y);
                    HitboxCoordinates[1] = new Vector2(x + 24 + 7, y);
                    HitboxCoordinates[2] = new Vector2(x + 24 + 7, y + length);
                    HitboxCoordinates[3] = new Vector2(x + 24, y + length);
                    break;

                case GlassOrientation.HTOP:
                    HitboxCoordinates[0] = new Vector2(x, y);
                    HitboxCoordinates[1] = new Vector2(x + length, y);
                    HitboxCoordinates[2] = new Vector2(x + length, y + 7);
                    HitboxCoordinates[3] = new Vector2(x, y + 7);
                    break;

                case GlassOrientation.HMID:
                    HitboxCoordinates[0] = new Vector2(x, y + 12);
                    HitboxCoordinates[1] = new Vector2(x + length, y + 12);
                    HitboxCoordinates[2] = new Vector2(x + length, y + 12 + 7);
                    HitboxCoordinates[3] = new Vector2(x, y + 12 + 7);
                    break;

                case GlassOrientation.HBOT:
                    HitboxCoordinates[0] = new Vector2(x, y + 24);
                    HitboxCoordinates[1] = new Vector2(x + length, y + 24);
                    HitboxCoordinates[2] = new Vector2(x + length, y + 24 + 7);
                    HitboxCoordinates[3] = new Vector2(x, y + 24 + 7);
                    break;
            }
        }

        public void Break()
        {
            IsBroken = true;
        }
    }

    public enum GlassOrientation
    {
        HTOP, HMID, HBOT,
        VLEFT, VMID, VRIGHT
    }
}
