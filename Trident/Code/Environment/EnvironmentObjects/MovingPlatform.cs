﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Code.Environment.EnvironmentObjects;
using Trident.Code.Utility;
using Trident.Controllable;
using Trident.DataObjects;
using Trident.Utility;

namespace Trident.Environment.EnvironmentObjects
{
    public class MovingPlatform : SwitchableState
    {
        //start / end positions of the top left coordinate
        public Vector2 StartPosition { get; }
        public Vector2 EndPosition { get; }

        //must begin with top-left coordinate, and proceed clockwise
        public Vector2[] HitboxCoordinates { get; private set; }
        public Vector2 TopLeftCoordinate { get { return HitboxCoordinates[0]; } }

        public readonly int Width;
        public const int Height = 32;

        public float Speed { get; set; }

        private readonly bool IsMovementHorizontal;

        public MovingPlatformState State { get; set; }
        public bool IsPaused { get; set; }

        private MovingPlatformState startingState;
        private bool startPaused;

        public Level Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startPosition">Tile position that the upper left corner starts on</param>
        /// <param name="endPosition">Tile position that the upper left corner ends on</param>
        /// <param name="speed">Speed the platform moves per frame. Only valid speeds are: .125, .25, .5, 1, 2, 4</param>
        /// <param name="width">Width of the platform. Should be multiple of 32.</param>
        /// <param name="state">The starting state of the platform.</param>
        public MovingPlatform(Level level, Vector2 startPosition, Vector2 endPosition, float speed, int width, MovingPlatformState state = MovingPlatformState.Forward, bool isPaused = false)
        {
            if(startPosition.X != endPosition.X && startPosition.Y != endPosition.Y)
            {
                throw new ArgumentException("Moving platforms must move strictly horizontal, or strictly vertical.");
            }

            if(width % 32 != 0)
            {
                throw new ArgumentException("Moving platform width must be a multiple of 32.");
            }

            if (speed != 0.0f && speed != .125f && speed != .25f && speed != .5f && speed != 1.0f && speed != 2.0f)
            {
                throw new ArgumentException("Moving platform speed must be one of the following: .125, .25, .5, 1, 2, 4");
            }

            StartPosition = startPosition;
            EndPosition = endPosition;
            Speed = Math.Abs(speed);
            Level = level;
            Width = width;
            State = state;
            IsPaused = isPaused;

            //used for re-initializing
            startingState = state;
            startPaused = isPaused;

            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(StartPosition.X, StartPosition.Y);
            HitboxCoordinates[1] = new Vector2(StartPosition.X + Width, StartPosition.Y);
            HitboxCoordinates[2] = new Vector2(StartPosition.X + Width, StartPosition.Y + Height);
            HitboxCoordinates[3] = new Vector2(StartPosition.X, StartPosition.Y + Height);

            IsMovementHorizontal = (StartPosition.Y == EndPosition.Y);
        }

        public void ToggleState()
        {
            IsPaused = !IsPaused;
        }

        /// <summary>
        /// Updates the position of the hitbox of the moving platform, as well
        /// as the state (if it is transitioning from moving forward to backward).
        /// 
        /// TODO: move the player that is on top of the platform or being pushed by the platform
        /// TODO: kill the player if they get crushed between moving platform and wall (or other platform)
        /// </summary>
        public void Update()
        {
            //Note: i'm blindly shifting in the direction the state tells me to,
            //THEN checking whether the state should be changed (for next frame).
            //this could cause (slightly) bad behavior if the STARTING state is opposite
            //the direction it really should be moving.
            if (IsPaused)
            {
                return;
            }
            else
            {
                Vector2 shiftVector = new Vector2(0, 0);

                if (State == MovingPlatformState.Forward)
                {
                    if (IsMovementHorizontal)
                    {
                        shiftVector = new Vector2(Speed, 0);
                        Shift(shiftVector);

                        if (TopLeftCoordinate.X >= EndPosition.X)
                        {
                            State = MovingPlatformState.Backward;
                        }
                    }
                    else
                    {
                        shiftVector = new Vector2(0, Speed);
                        Shift(shiftVector);

                        if (TopLeftCoordinate.Y >= EndPosition.Y)
                        {
                            State = MovingPlatformState.Backward;
                        }
                    }
                }
                else if (State == MovingPlatformState.Backward)
                {
                    if (IsMovementHorizontal)
                    {
                        shiftVector = new Vector2(-Speed, 0);
                        Shift(shiftVector);

                        if (TopLeftCoordinate.X <= StartPosition.X)
                        {
                            State = MovingPlatformState.Forward;
                        }
                    }
                    else
                    {
                        shiftVector = new Vector2(0, -Speed);
                        Shift(shiftVector);

                        if (TopLeftCoordinate.Y <= StartPosition.Y)
                        {
                            State = MovingPlatformState.Forward;
                        }
                    }
                }

                //apply shift vector to all players, as well as PlayerGrenade's PulseGrenade
                for(int i = 0; i <= Level.PlayerArray.Length; i++)
                {
                    Movable item = null;

                    if (i < Level.PlayerArray.Length)
                    {
                        item = Level.PlayerArray[i];
                    }
                    else
                    {
                        item = Level.ActiveGrenade;

                        if (item == null)
                        {
                            continue;
                        }
                    }

                    if ((item.Hitbox.X >= HitboxCoordinates[0].X && item.Hitbox.X <= HitboxCoordinates[1].X) || (item.Hitbox.X + item.Hitbox.Width >= HitboxCoordinates[0].X && item.Hitbox.X + item.Hitbox.Width <= HitboxCoordinates[1].X))
                    {

                        //this deals with the case where the player is standing on the platform after it has updated.
                        //it handles Horizontal (on platform) and Vertical movement (on platform)
                        if (item.Hitbox.Y + item.Hitbox.Height <= HitboxCoordinates[0].Y + 5 && item.Hitbox.Y + item.Hitbox.Height >= HitboxCoordinates[0].Y - 5)
                        {
                            item.ShiftDueToMovingPlatform(shiftVector);
                        }
                    }
                        
                    //if any of the above cases were true, this if should evaluate to false, since the player
                    //gets pushed to a spot where they won't intersect with the rectangle.

                    //however, there are still the cases of being PUSHED L/R by a horizontal platform, or downward
                    //by a vertical platform. This case handles those. NOTE: player getting crushed logic is handled
                    //within the ShiftDueToMovingPlatform method
                    if (CollisionDetection.Intersects(HitboxCoordinates, item.Hitbox.Coordinates))
                    {
                            item.ShiftDueToMovingPlatform(shiftVector);
                    }
                }
            }
        }

        private void Shift(Vector2 shiftAmount)
        {
            for (int i = 0; i < HitboxCoordinates.Length; i++ )
            {
                HitboxCoordinates[i].X += shiftAmount.X;
                HitboxCoordinates[i].Y += shiftAmount.Y;
            }
        }

        public void Reinitialize()
        {
            State = startingState;
            IsPaused = startPaused;

            HitboxCoordinates[0] = new Vector2(StartPosition.X, StartPosition.Y);
            HitboxCoordinates[1] = new Vector2(StartPosition.X + Width, StartPosition.Y);
            HitboxCoordinates[2] = new Vector2(StartPosition.X + Width, StartPosition.Y + Height);
            HitboxCoordinates[3] = new Vector2(StartPosition.X, StartPosition.Y + Height);
        }
    
        public void Draw(SpriteBatch _spritebatch, GameCamera camera)
        {
            Vector2 cameraTopLeft = new Vector2(camera.Position.X - camera.Width / 2, camera.Position.Y - camera.Height / 2);

            float drawX = HitboxCoordinates[0].X;
            float drawY = HitboxCoordinates[0].Y;

            //TODO: this is kinda a hacky way to draw the moving platform
            for(int i = 0; i < Width; i += 32)
            {
                _spritebatch.Draw(Sprites.GroundTile, new Vector2(drawX + i, drawY) - cameraTopLeft, Color.White);
            }
        }
    }



    public enum MovingPlatformState
    {
        Forward,
        Backward
    }
}
