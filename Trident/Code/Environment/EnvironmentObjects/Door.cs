﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Code.Environment.EnvironmentObjects
{
    public class Door : SwitchableState
    {
        public bool IsOpen { get; set; }
        public Vector2[] HitboxCoordinates { get; private set; }

        public Door(float x, float y, float width, float height, bool isOpen = false)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x, y); 
            HitboxCoordinates[1] = new Vector2(x + width - 1, y);
            HitboxCoordinates[2] = new Vector2(x + width - 1, y + height - 1);
            HitboxCoordinates[3] = new Vector2(x, y + height - 1);

            IsOpen = isOpen;
        }

        public void ToggleState()
        {
            IsOpen = !IsOpen;
        }
    }
}
