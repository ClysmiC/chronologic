﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Trident.DataObjects;
using Trident.Controllable;
using Trident.Utility;
using Trident.Code.Utility;
using Trident.Code.Environment.EnvironmentObjects;

namespace Trident.Environment.EnvironmentObjects
{
    public abstract class Tile
    {
        //hitbox coordinates relative to X, Y position of Tile
        public abstract Vector2[] HitboxCoordinates { get; }
        public abstract Texture2D Sprite { get; }

        public virtual bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {
            return !CollisionDetection.Intersects(HitboxCoordinates, hitbox.Coordinates);

        }

        /// <summary>
        /// Point-in-polygon test. Taken from http://dominoc925.blogspot.com/2012/02/c-code-snippet-to-determine-if-point-is.html
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IntersectsWithPoint(Vector2 point)
        {
            bool isInside = false;
            for (int i = 0, j = HitboxCoordinates.Length - 1; i < HitboxCoordinates.Length; j = i++)
            {
                if (((HitboxCoordinates[i].Y > point.Y) != (HitboxCoordinates[j].Y > point.Y)) &&
                (point.X < (HitboxCoordinates[j].X - HitboxCoordinates[i].X) * (point.Y - HitboxCoordinates[i].Y) / (HitboxCoordinates[j].Y - HitboxCoordinates[i].Y) + HitboxCoordinates[i].X))
                {
                    isInside = !isInside;
                }
            }
            return isInside;
        }
    }


    //Todo-- make Empty Tiles just null? that would store a lot less in the _leveldata array
    public class EmptyTile : Tile
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return null; } } //TODO: refactor this

        public EmptyTile()
        {
            HitboxCoordinates = new Vector2[0];
        }

        public override bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {
            return true;
        }
    }

    public class GroundTile : Tile
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.GroundTile; } }

        public GroundTile(float x, float y)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x, y);
            HitboxCoordinates[1] = new Vector2(x + 31, y);
            HitboxCoordinates[2] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[3] = new Vector2(x, y + 31);
        }
    }

    public class OneWayTile : Tile
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.OneWayTile; } }

        public OneWayTile(float x, float y)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x, y);
            HitboxCoordinates[1] = new Vector2(x + 31, y);
            HitboxCoordinates[2] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[3] = new Vector2(x, y + 31);
        }

        public override bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {
            if (item.YSpeed < 0 || hitbox.Y + hitbox.Height - 1 > HitboxCoordinates[0].Y)
            {
                return true;
            }

            return base.IsPositionFreeForMovableItem(hitbox, item);
        }
    }

    public class ButtonTile : Tile
    {
        private Button _button;

        public override Texture2D Sprite { get { return _button.IsPressed ? Sprites.ButtonDownTile : Sprites.ButtonUpTile; }}

        public override Vector2[] HitboxCoordinates { get { return _button.HitboxCoordinates; } }

        public ButtonTile(Button button)
        {
            _button = button;
        }
    }

    public class DoorTile : Tile
    {
        private Door _door;

        public override Texture2D Sprite { get { return _door.IsOpen ? null : Sprites.Door; }}

        public override Vector2[] HitboxCoordinates { get { return _door.HitboxCoordinates; } }

        public DoorTile(Door door)
        {
            _door = door;
        }

        public override bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {
            if (_door.IsOpen)
            {
                return true;
            }
            else
            {
                return base.IsPositionFreeForMovableItem(hitbox, item);
            }
        }
    }

    public class GlassTile : Tile
    {
        private GlassPane _glass;

        public override Vector2[] HitboxCoordinates { get { return _glass.HitboxCoordinates; } }

        private Texture2D _sprite;
        public override Texture2D Sprite { get { return (_glass.IsBroken) ? null : _sprite; } }


        public GlassTile(GlassPane glass)
        {
            _glass = glass;

            switch (_glass.Orientation)
            {
                case GlassOrientation.VLEFT:
                    _sprite = Sprites.GlassTileVLeft;
                    break;

                case GlassOrientation.VMID:
                    _sprite = Sprites.GlassTileVMid;
                    break;

                case GlassOrientation.VRIGHT:
                    _sprite = Sprites.GlassTileVRight;
                    break;

                case GlassOrientation.HTOP:
                    _sprite = Sprites.GlassTileHTop;
                    break;

                case GlassOrientation.HMID:
                    _sprite = Sprites.GlassTileHMid;
                    break;

                case GlassOrientation.HBOT:
                    _sprite = Sprites.GlassTileHBot;
                    break;
            }
        }

        public override bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {
            if (_glass.IsBroken == true)
            {
                return true;
            }

            if (!_glass.IsBroken && ((_glass.IsHorizontal == false && Math.Abs(item.XSpeed) > Constants.GlassBreakSpeed) || (_glass.IsHorizontal == true && Math.Abs(item.YSpeed) > Constants.GlassBreakSpeed)))
            {
                _glass.Break();
                return true;
            }

            return base.IsPositionFreeForMovableItem(hitbox, item);
        }
    }

    public class SwitchButtonTile : EmptyTile
    {
        public override Texture2D Sprite { get { return Sprites.Switch; } }

        private SwitchButton _switchButton;

        public SwitchButtonTile(SwitchButton switchButton)
        {
            _switchButton = switchButton;
        }
    }

    /// <summary>
    /// A tile that is part of the "track" for a moving platform. This tile is not the platform itself--
    /// it is essentially an empty tile, but it "supports" a MovingPlatform object that may enter the tile.
    /// Since collision detection is done by tiles, all tiles that support a MovingPlatform have 
    /// HitboxCoordinates that simply point to the MovingPlatform's current hitbox. This is a bit limiting, as 
    /// it means that the paths of 2 seperate MovingPlatforms cannot include the same tiles, but it
    /// is enough functionality for this game.
    /// </summary>
    public class MovingPlatformTile : Tile
    {
        private MovingPlatform _platform; //the underlying platform that this "tile" supports

        public MovingPlatform Platform
        { get { return _platform; } set { _platform = value; } }

        public override Vector2[] HitboxCoordinates { get { return _platform.HitboxCoordinates; } }

        //drawing isn't done by this tile, since the tile might not actually contain the platform
        //at any given instance.
        //thus the drawing is delegated to the actual platform
        public override Texture2D Sprite { get { return null; } } //TODO: refactor this

        public MovingPlatformTile(MovingPlatform platform)
        {
            _platform = platform;
        }

    }

    public abstract class Ramp : Tile{ }

    public class RampRightTile : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.RampRightTile; } }

        public RampRightTile(float x, float y)
        {
            HitboxCoordinates = new Vector2[3];
            HitboxCoordinates[0] = new Vector2(x + 31, y);
            HitboxCoordinates[1] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[2] = new Vector2(x, y + 31);
        }
    }

    public class HalfRampRightTile1 : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.HalfRampRightTile1; } }

        public HalfRampRightTile1(float x, float y)
        {
            HitboxCoordinates = new Vector2[3];
            HitboxCoordinates[0] = new Vector2(x + 31, y + 16);
            HitboxCoordinates[1] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[2] = new Vector2(x, y + 31);
        }
    }

    public class HalfRampRightTile2 : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.HalfRampRightTile2; } }

        public HalfRampRightTile2(float x, float y)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x + 31, y);
            HitboxCoordinates[1] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[2] = new Vector2(x, y + 31);
            HitboxCoordinates[3] = new Vector2(x, y + 15);
        }
    }

    public class RampLeftTile : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.RampLeftTile; } }

        public RampLeftTile(float x, float y)
        {
            HitboxCoordinates = new Vector2[3];
            HitboxCoordinates[0] = new Vector2(x, y);
            HitboxCoordinates[1] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[2] = new Vector2(x, y + 31);
        }
    }

    public class HalfRampLeftTile1 : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.HalfRampLeftTile1; } }

        public HalfRampLeftTile1(float x, float y)
        {
            HitboxCoordinates = new Vector2[3];
            HitboxCoordinates[0] = new Vector2(x, y + 16);
            HitboxCoordinates[1] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[2] = new Vector2(x, y + 31);
        }
    }

    public class HalfRampLeftTile2 : Ramp
    {
        public override Vector2[] HitboxCoordinates { get; }
        public override Texture2D Sprite { get { return Sprites.HalfRampLeftTile2; } }

        public HalfRampLeftTile2(float x, float y)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x, y);
            HitboxCoordinates[1] = new Vector2(x, y + 31);
            HitboxCoordinates[2] = new Vector2(x + 31, y + 31);
            HitboxCoordinates[3] = new Vector2(x + 31, y + 15);
        }
    }
}
