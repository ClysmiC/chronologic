﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Controllable;
using Trident.Environment;

namespace Trident.Code.Environment.EnvironmentObjects
{
    public class Button
    {
        public bool IsPressed { get; set; }
        public Vector2[] HitboxCoordinates { get; set; }
        private Level Level { get; set; }

        private bool pressedLastFrame;

        private SwitchableState[] targets;


        public Button(Level level, float x, float y, float width, float height, SwitchableState[] targets)
        {
            HitboxCoordinates = new Vector2[4];
            HitboxCoordinates[0] = new Vector2(x, y);
            HitboxCoordinates[1] = new Vector2(x + width - 1, y);
            HitboxCoordinates[2] = new Vector2(x + width - 1, y + height - 1);
            HitboxCoordinates[3] = new Vector2(x, y + height - 1);

            Level = level;
			
            this.targets = targets;
            pressedLastFrame = false;
        }

        public void Update()
        {
            //assume false, until shown to be true
            IsPressed = false;

            //detect item is on platform
            for (int i = 0; i <= Level.PlayerArray.Length; i++)
            {
                Movable item = null;

                if (i < Level.PlayerArray.Length)
                {
                    item = Level.PlayerArray[i];
                }
                else
                {
                    item = Level.ActiveGrenade;

                    if (item == null)
                    {
                        continue;
                    }
                }

                if ((item.Hitbox.X >= HitboxCoordinates[0].X && item.Hitbox.X <= HitboxCoordinates[1].X) || (item.Hitbox.X + item.Hitbox.Width >= HitboxCoordinates[0].X && item.Hitbox.X + item.Hitbox.Width <= HitboxCoordinates[1].X))
                {
                    if (item.Hitbox.Y + item.Hitbox.Height <= HitboxCoordinates[0].Y + 1 && item.Hitbox.Y + item.Hitbox.Height >= HitboxCoordinates[0].Y - 1)
                    {
                        IsPressed = true;
                        break; //once one item is on the button, no need to check for more.
                    }
                }
            }


            if (pressedLastFrame != IsPressed)
            {
                foreach(SwitchableState target in targets)
				{
                    target.ToggleState();
				}
            }

            pressedLastFrame = IsPressed;
        }
    }
}
