﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.DataObjects;

namespace Trident.Code.Environment.EnvironmentObjects
{
    public class SwitchButton
    {
        public bool IsPressed { get; set; }
        public RectangleF Hitbox { get; set; }
        

        private SwitchableState[] targets;

        public SwitchButton(float x, float y, SwitchableState[] targets)
        {
            Hitbox = new RectangleF(x, y, 32, 32);
            IsPressed = false;
            this.targets = targets;
        }

        public void Press()
        {
            foreach(SwitchableState target in targets)
            {
                target.ToggleState();
            }
        }
    }
}
