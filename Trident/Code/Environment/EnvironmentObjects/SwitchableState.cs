﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Code.Environment.EnvironmentObjects
{
    public interface SwitchableState
    {
        void ToggleState();
    }
}
