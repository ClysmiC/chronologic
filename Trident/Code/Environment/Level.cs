﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using Trident.Environment;
using Trident.Utility;
using Trident.Controllable;
using Trident.DataObjects;
using Trident.Environment.EnvironmentObjects;
using Trident.Code.Environment.EnvironmentObjects;
using Trident.Code.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Trident.Input;

namespace Trident.Environment
{
    public class Level
    {
        public readonly string Name;

        public readonly Tile[,] LevelData;
        public readonly MovingPlatform[] MovingPlatforms;
        public readonly Button[] Buttons;
        public readonly Door[] Doors;
        public readonly SwitchButton[] SwitchButtons;
        public readonly GlassPane[] GlassPanes;
        public readonly Rectangle Bounds;
        public readonly int MaxFramesForLevel;

        public int Height { get { return LevelData.GetLength(0) * 32; } }
        public int Width { get { return LevelData.GetLength(1) * 32; } }


        public long CurrentFrame
        {
            get { return FrameCounter.TotalFrames - startFrame; }
        }

        private long startFrame;

        public Player Player { get; set; }
        public PulseGrenade ActiveGrenade { get; set; }

        public GameCamera Camera { get; set; }

        //stores the indices of a reordering of the PlayerArray, with the players
        //ordered chronologically from earliest controlled to latest controlled
        //this priority keeps new player from "stealing" throws from two older copies
        //that succesfully did a throw
        public int[] PlayerIdChronological { get; }

        public bool IsLive { get; set; } //whether or not the level is "live", i.e., the player can be controlled

        public Player[] PlayerArray { get; private set; }
        public int PlayerId { get; private set; }

        public int MaxPlayerCopyCount{get; private set;}

        private Vector2 startingPosition;

        /// <summary>
        /// Constructor for a Level.
        /// </summary>
        /// <param name="filename">The name of the file to load the level from.</param>
        public Level(string filename)
        {
			MaxFramesForLevel = 100 * FrameCounter.TargetFps;
			
            string str = System.IO.File.ReadAllText(filename);
            JObject obj = JObject.Parse(str);

            Name = (string)obj["name"];

            JToken pos = (JToken)obj["startingPosition"];
            float playerStartX = (float)pos["x"];
            float playerStartY = (float)pos["y"] - 4; //minus 4 because player is 36 pixels tall (not 32)
            startingPosition = new Vector2(playerStartX, playerStartY);

            //extract movingplatforms, buttons, etc. before the tiles, since special tiles need to refer to
            //these objects


            //temporary lists for storing these as we read them in. ultimately they get copied
            //into arrays that are member data for the Level object. The lists are a bit easier to
            //work with but they aren't as efficient so I only use them in the constructor then copy
            //to array
            List<MovingPlatform> movingPlatforms = new List<MovingPlatform>();
            List<Door> doors = new List<Door>();
            List<Button> buttons = new List<Button>();
            List<SwitchButton> switchButtons = new List<SwitchButton>();
            List<GlassPane> glassPanes = new List<GlassPane>();

            /******MOVING PLATFORMS******/
            {
                JArray array = (JArray)obj["movingplatforms"];
                foreach (JToken item in array)
                {
                    float platformStartX = (float)item["startX"];
                    float platformStartY = (float)item["startY"];
                    float endX = (float)item["endX"];
                    float endY = (float)item["endY"];
                    float speed = (float)item["speed"];
                    int width = (int)item["width"];
                    string state = (string)item["state"];
                    bool isPaused = (bool)item["ispaused"];

                    movingPlatforms.Add
                    (
                        new MovingPlatform
                        (
                            this,
                            new Vector2(platformStartX, platformStartY),
                            new Vector2(endX, endY),
                            speed,
                            width,
                            (MovingPlatformState)Enum.Parse(typeof(MovingPlatformState), state),
                            isPaused
                        )
                    );
                }
            }

            /******DOORS******/
            {
                JArray array = (JArray)obj["doors"];
                foreach (JToken item in array)
                {
                    float x = (float)item["x"];
                    float y = (float)item["y"];
                    float width = (float)item["width"];
                    float height = (float)item["height"];
                    bool isOpen = (bool)item["isOpen"];

                    doors.Add
                    (
                        new Door
                        (
                            x,
                            y,
                            width,
                            height,
                            isOpen
                        )
                    );
                }
            }

            /******BUTTONS******/
            {
                JArray array = (JArray)obj["buttons"];
                foreach (JToken item in array)
                {
                    float x = (float)item["x"];
                    float y = (float)item["y"];
                    float width = (float)item["width"];
                    float height = (float)item["height"];
                    JArray targetPlatformIds = (JArray)item["targetplatforms"];
                    JArray targetDoorIds = (JArray)item["targetdoors"];

                    List<MovingPlatform> targetPlatforms = new List<MovingPlatform>();
                    for (int i = 0; i < targetPlatformIds.Count; i++)
                    {
                        int id = (int)targetPlatformIds[i];
                        targetPlatforms.Add(movingPlatforms.ElementAt(id));
                    }

                    List<Door> targetDoors = new List<Door>();
                    for (int i = 0; i < targetDoorIds.Count; i++)
                    {
                        int id = (int)targetPlatformIds[i];
                        targetDoors.Add(doors.ElementAt(id));
                    }

                    List<SwitchableState> targets = new List<SwitchableState>();
                    targets.AddRange(targetPlatforms);
                    targets.AddRange(targetDoors);

                    buttons.Add
                    (
                        new Button
                        (
                            this,
                            x,
                            y,
                            width,
                            height,
                            targets.ToArray()
                        )
                    );
                }
            }
            

            /******SWITCH BUTTONS******/
            {
                JArray array = (JArray)obj["switchbuttons"];
                foreach (JToken item in array)
                {
                    float x = (float)item["x"];
                    float y = (float)item["y"];
                    JArray targetPlatformIds = (JArray)item["targetplatforms"];
                    JArray targetDoorIds = (JArray)item["targetdoors"];

                    List<MovingPlatform> targetPlatforms = new List<MovingPlatform>();
                    for (int i = 0; i < targetPlatformIds.Count; i++)
                    {
                        int id = (int)targetPlatformIds[i];
                        targetPlatforms.Add(movingPlatforms.ElementAt(id));
                    }

                    List<Door> targetDoors = new List<Door>();
                    for (int i = 0; i < targetDoorIds.Count; i++)
                    {
                        int id = (int)targetPlatformIds[i];
                        targetDoors.Add(doors.ElementAt(id));
                    }

                    List<SwitchableState> targets = new List<SwitchableState>();
                    targets.AddRange(targetPlatforms);
                    targets.AddRange(targetDoors);

                    switchButtons.Add
                    (
                        new SwitchButton
                        (
                            x,
                            y,
                            targets.ToArray()
                        )
                    );
                }
            }

            /******GLASS PANES******/
            {
                JArray array = (JArray)obj["glasspanes"];
                foreach (JToken item in array)
                {
                    float x = (float)item["x"];
                    float y = (float)item["y"];
                    string orientation = (string)item["orientation"];
                    float length = (float)item["length"];

                    glassPanes.Add
                    (
                        new GlassPane
                        (
                            x,
                            y,
                            (GlassOrientation)Enum.Parse(typeof(GlassOrientation), orientation),
                            length
                        )
                    );
                }
            }



            MovingPlatforms = movingPlatforms.ToArray();
            Buttons = buttons.ToArray();
            Doors = doors.ToArray();
            SwitchButtons = switchButtons.ToArray();
            GlassPanes = glassPanes.ToArray();

            JArray level = (JArray)obj["leveldata"];

            LevelData = new Tile[level.Count, ((JArray)level[0]).Count];

            for (int row = 0; row < level.Count; row++)
            {
                JArray levelRow = (JArray)level[row];

                for (int column = 0; column < levelRow.Count; column++)
                {
                    JToken tile = levelRow[column];
                    string tileType = (string)tile["type"];

                    float x = column * 32;
                    float y = row * 32;

                    switch(tileType)
                    {
                        case "empty":
                        {
                            LevelData[row, column] = new EmptyTile();
                            break;
                        }

                        case "ground":
                        {
                            LevelData[row, column] = new GroundTile(x, y);
                            break;
                        }

                        case "oneway":
                        {
                            LevelData[row, column] = new OneWayTile(x, y);
                            break;
                        }

                        case "button":
                        {
                            int id = (int)tile["id"];
                            LevelData[row, column] = new ButtonTile(Buttons[id]);
                            break;
                        }

                        case "door":
                        {
                            int id = (int)tile["id"];
                            LevelData[row, column] = new DoorTile(Doors[id]);
                            break;
                        }

                        case "glass":
                        {
                            int id = (int)tile["id"];
                            LevelData[row, column] = new GlassTile(GlassPanes[id]);
                            break;
                        }

                        case "switchbutton":
                        {
                            int id = (int)tile["id"];
                            LevelData[row, column] = new SwitchButtonTile(SwitchButtons[id]);
                            break;
                        }

                        case "movingplatform":
                        {
                            int id = (int)tile["id"];
                            LevelData[row, column] = new MovingPlatformTile(MovingPlatforms[id]);
                            break;
                        }

                        case "rampright":
                        {
                            LevelData[row, column] = new RampRightTile(x, y);
                            break;
                        }

                        case "halframpright1":
                        {
                            LevelData[row, column] = new HalfRampRightTile1(x, y);
                            break;
                        }

                        case "halframpright2":
                        {
                            LevelData[row, column] = new HalfRampRightTile2(x, y);
                            break;
                        }

                        case "rampleft":
                        {
                            LevelData[row, column] = new RampLeftTile(x, y);
                            break;
                        }

                        case "halframpleft1":
                        {
                            LevelData[row, column] = new HalfRampLeftTile1(x, y);
                            break;
                        }

                        case "halframpleft2":
                        {
                            LevelData[row, column] = new HalfRampLeftTile2(x, y);
                            break;
                        }
                    }
                }
            }

            IsLive = false;

            MaxPlayerCopyCount = 4;
            PlayerArray = new Player[MaxPlayerCopyCount];
            PlayerIdChronological = new int[] { 0, 1, 2, 3 };
            PlayerId = 0;         

            PlayerArray[0] = new PlayerFast(this, startingPosition);
            PlayerArray[1] = new PlayerGrenade(this, startingPosition);
            PlayerArray[2] = new PlayerSplit(this, startingPosition);
            PlayerArray[3] = new Player3(this, startingPosition);

            Player = PlayerArray[0];


            Camera = new GameCamera((int)Player.positionOfCenter().X, (int)Player.positionOfCenter().Y, Resolution.VWidth, Resolution.VHeight);
            startFrame = FrameCounter.TotalFrames;
        }

        public void Load(ContentManager contentManager)
        {
            Player.Load(contentManager);
        }

        public Tile TileAt(Vector2 position)
        {
            return TileAt(position.X, position.Y);
        }

        public Tile TileAt(float x, float y)
        {
            int row = (int)(y / 32);
            int column = (int)(x / 32);

            return LevelData[row, column];
        }

        public void Update(InputState input)
        {
            if(IsLive)
            {

                if(input.isPressed(input.endLife))
                {
                    FinishIteration(true);
                }
                else if(input.isPressed(input.idle))
                {
                    Player.FillRemainingInputRecordWithIdle();
                    FinishIteration(false);
                }
                else if (CurrentFrame >= MaxFramesForLevel)
                {
                    FinishIteration(false);
                }
                else
                {
                    //earlier copies get priority, to keep newer copies from _accidentally_ rewriting the past!
                    for (int i = 0; i < PlayerArray.Length; i++)
                    {
                        //get the index of the player that should has
                        //the i'th priority
                        int index = PlayerIdChronological[i];

                        //don't worry about passing the input to a copy.
                        //the copy will just ignore it and go off their inputRecord
                        PlayerArray[index].Update(input);
                    }

                    foreach(MovingPlatform platform in MovingPlatforms)
                    {
                        platform.Update();
                    }

                    foreach(Button button in Buttons)
                    {
                        button.Update();
                    }

                    if (ActiveGrenade != null && ActiveGrenade.Exploded)
                    {
                        ActiveGrenade = null;
                    }
                
                    Camera.Position = Player.positionOfCenter();
                }
            }
            else
            {
                //control selector
                if(input.isPressed(input.jump))
                {
                    BeginNextIteration();
                }
                else if(input.isPressed(input.right))
                {
                    PlayerId = Math.Min(PlayerId + 1, MaxPlayerCopyCount - 1);
                }
                else if (input.isPressed(input.left))
                {
                    PlayerId = Math.Max(PlayerId - 1, 0);
                }
            }
        }

        /// <summary>
        /// This iteration is finished, either from time expiring, the player choosing to idle for the rest of the timer,
        /// or the player aborting the iteration. The parameter aborted determines whether the input should be saved
        /// and played back next time, or whether the player aborted the iteration.
        /// </summary>
        private void FinishIteration(bool aborted)
        {
            Player.UsedInLevel = !aborted;
            IsLive = false;


            foreach (Player player in PlayerArray)
            {
                //priority of reinitializing doesn't matter
                //so no need to use PlayerIdChronological
                player.Reinitialize();
            }

            foreach(MovingPlatform platform in MovingPlatforms)
            {
                platform.Reinitialize();
            }

            //unbreak glass
            foreach(GlassPane glass in GlassPanes)
            {
                glass.IsBroken = false;   
            }

            //suggest the user use the next player by id.
            //they can change this of course.
            if(!aborted)
            {
                PlayerId = (PlayerId + 1) % MaxPlayerCopyCount;
            }

            Camera.Position = new Vector2((int)Player.positionOfCenter().X, (int)Player.positionOfCenter().Y);
        }

        private void BeginNextIteration()
        {
            Player = PlayerArray[PlayerId];
            Player.IsActivePlayer = true;
            IsLive = true;
            startFrame = FrameCounter.TotalFrames + 1; // + 1 because the level doesn't "start" until next frame!

            //reorder the chronological priority
            //put the now-active player at the end
            bool found = false;
            for (int i = 0; i < PlayerIdChronological.Length; i++)
            {
                if(found) //shift indices down 1 to take up space of the id we are "removing" and putting at the end
                {
                    PlayerIdChronological[i - 1] = PlayerIdChronological[i];

                    //put the now-active player id at the end
                    if(i == PlayerIdChronological.Length - 1)
                    {
                        PlayerIdChronological[i] = PlayerId;
                    }
                }

                if(PlayerIdChronological[i] == PlayerId)
                {
                    found = true;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int tileWidth = 32;
            int tileHeight = 32;

            Vector2 cameraTopLeft = new Vector2(Camera.Position.X - Camera.Width / 2, Camera.Position.Y - Camera.Height / 2);

            DrawBackground(spriteBatch);

            for(int down = 0; down < LevelData.GetLength(0); down++)
            {
                for (int across = 0; across < LevelData.GetLength(1); across++)
                {
                    if(LevelData[down, across].Sprite != null)
                    {
                        spriteBatch.Draw(LevelData[down, across].Sprite, new Vector2(across * tileWidth, down * tileHeight) - cameraTopLeft, Color.White);
                    }
                }
            }

            //draw moving platforms
            foreach(MovingPlatform platform in MovingPlatforms)
            {
                platform.Draw(spriteBatch, Camera);
            }

            //draw selected player at starting position
            if(!IsLive)
            {
                spriteBatch.Draw(
                    Sprites.SpritePlayer[PlayerId],
                    startingPosition - cameraTopLeft,
                    Color.White
                );
            }
            else
            {
                //draw all copies
                for (int i = 0; i < PlayerArray.Length; i++ )
                {
                    int index = PlayerIdChronological[i];

                    PlayerArray[index].Draw(spriteBatch, Camera);
                }
            }


            DrawPlayerSelector(spriteBatch);

            float secondsLeft = (MaxFramesForLevel - CurrentFrame) / (float)FrameCounter.TargetFps;

            if(IsLive)
            {
                UiDrawer.DrawString(spriteBatch, secondsLeft.ToString("##.##"), new Vector2(700, 30), Color.White);
                UiDrawer.DrawString(spriteBatch, "Move: arrows / left joystick | Jump: Spacebar / A | Sprint: Shift / RT | Throw: WASD / right joystick", new Vector2(40, 750), Color.White);

                UiDrawer.DrawString(spriteBatch, "CameraX: " + Camera.Position.X, new Vector2(0, 60), Color.White);
                UiDrawer.DrawString(spriteBatch, "CameraY: " + Camera.Position.Y, new Vector2(0, 90), Color.White);
            }
        }

        public void DrawBackground(SpriteBatch spriteBatch)
        {
            Texture2D bg = Sprites.Background;
            Vector2 cameraTopLeft = new Vector2(Camera.Position.X - Camera.Width / 2, Camera.Position.Y - Camera.Height / 2);

            for(int down = 0; down < Height; down += bg.Height)
            {
                for (int across = 0; across < Width; across += bg.Width)
                {
                    spriteBatch.Draw(
                        bg, //Texture
                        new Vector2(across, down) - cameraTopLeft, //position
                        //Source Rect from texture (prevent overdrawing near bottom and left),
                        new Rectangle(0, 0, Math.Min(bg.Width, Width - across), Math.Min(bg.Height, Height - down)),
                        Color.White
                    );
                }
            }
        }

        public void DrawPlayerSelector(SpriteBatch spriteBatch)
        {
            Texture2D selector = Sprites.PlayerSelector;
            Texture2D highlight = Sprites.PlayerSelectedHighlight;
            int offsetPerPlayer = highlight.Width; //amount the highlight shifts based on the player index

            //draw selector
            spriteBatch.Draw(
                selector,
                //10px is padding
                new Vector2(Resolution.VWidth - selector.Width - 10, Resolution.VHeight - selector.Height - 10),
                Color.White
            );

            //draw highlight around selected player
            spriteBatch.Draw(
                highlight,
                //10px is padding
                new Vector2(Resolution.VWidth - selector.Width - 10 + offsetPerPlayer * PlayerId, Resolution.VHeight - selector.Height - 10),
                Color.White
            );
        }

        public void ToggleSwitchWithinRegion(RectangleF region)
        {
            foreach(SwitchButton switchz in SwitchButtons)
            {
                if(CollisionDetection.Intersects(region.Coordinates, switchz.Hitbox.Coordinates))
                {
                    switchz.Press();
                    return;
                }
            }
        }

        /// <summary>
        /// Determines if the character can move to the specified Rectangle.
        /// </summary>
        /// <param name="hitbox">The rectangle that the character would like to occupy</param>
        /// <param name="player">The player. This parameter is needed because one-way platforms need to know the character's Y-speed</param>
        /// <returns></returns>
        public bool IsPositionFreeForMovableItem(RectangleF hitbox, Movable item)
        {

            //top-left
            int column = (int) (hitbox.X / 32);
            int row = (int) (hitbox.Y / 32);

            Tile tile1 = LevelData[row, column];


            //top-right
            column = (int)((hitbox.X + hitbox.Width - 1) / 32);
            row = (int)(hitbox.Y / 32);

            Tile tile2 = LevelData[row, column];


            //bottom-right
            column = (int)((hitbox.X + hitbox.Width - 1) / 32);
            row = (int)((hitbox.Y + hitbox.Height - 1) / 32);
            
            Tile tile3 = LevelData[row, column];


            //bottom-left
            column = (int)(hitbox.X / 32);
            row = (int)((hitbox.Y + hitbox.Height - 1) / 32);

            Tile tile4 = LevelData[row, column];


            //mid-left
            column = (int)(hitbox.X / 32);
            row = (int)((hitbox.Y + hitbox.Height / 2) / 32);

            Tile tile5 = LevelData[row, column];


            //mid-right
            column = (int)((hitbox.X + hitbox.Width - 1) / 32);
            row = (int)((hitbox.Y + hitbox.Height / 2) / 32);

            Tile tile6 = LevelData[row, column];

            //Optimize: check for repeat tiles
            //Hack: maybe come up with a better way of grabbing all tiles the player is touching, instead
            //of just grabbing 6 critical points around the hitbox
            return tile1.IsPositionFreeForMovableItem(hitbox, item) && tile2.IsPositionFreeForMovableItem(hitbox, item) && tile3.IsPositionFreeForMovableItem(hitbox, item) && tile4.IsPositionFreeForMovableItem(hitbox, item) && tile5.IsPositionFreeForMovableItem(hitbox, item) && tile6.IsPositionFreeForMovableItem(hitbox, item);
        }

        /// <summary>
        /// Returns whether or not a Ramp tile lies directly the point (x, y), by checking
        /// all points below the specified point up until maxDist is reached. If a ramp is found
        /// at any of the points, the method returns true.
        /// </summary>
        /// <param name="x">The X value of the coordinate being checked.</param>
        /// <param name="y">The Y value of the coordinate being checked.</param>
        /// <param name="maxDist">The amount of points to check below the specified point.</param>
        /// <returns></returns>
        public bool IsRampBelow(float x, float y, float maxDist)
        { 
            //since we are checking directly below the character,
            //all potential tiles we check will be in the same column
            int column = (int)(x / 32);
            int row;
            int rowLastIteration = -100000;

            //check from maxDist -> 0
            //since ramps angle away from the player, it will probably be faster to detect a ramp if
            //we check further from the player and then come inwards.
            for(int i = (int)Math.Round(y + maxDist); i >= y; i--)
            {
                row = (int)(i / 32);

                if (row == rowLastIteration)
                {
                    //this pixel is on the same tile as last iteration,
                    //no need to re-check the tile
                    //realistically, only 1 or 2 tiles should get checked per method call,
                    //so this is a good optimization
                    continue;
                }

                if (LevelData[row, column] is Ramp)
                {
                    return true;
                }

                rowLastIteration = row;
            }

            return false;
        }
    }
}
