﻿using Trident.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using Trident.Environment;
using Trident.Code.Menu;
using System.Reflection;
using Trident.Input;

namespace Trident
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameClient : Game
    {
        // Don't use an auto-property for this because we pass graphics as a ref parameter somewhere.
        private GraphicsDeviceManager _graphics;
        public GraphicsDeviceManager Graphics
        {
            get { return _graphics; }
            set { _graphics = value; }
        }

        public SpriteBatch SpriteBatch { get; set; }
        public Level CurrentLevel { get; set; }
        public InputState CurrentInputState { get; set; }

        public GameState State;
        public MainMenu Menu;


        public GameClient()
        {
            Graphics = new GraphicsDeviceManager(this);
            Resolution.Init(ref _graphics);
            Resolution.SetVirtualResolution(1024, 768);
            Resolution.SetResolution(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width, GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height, false);

            Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Graphics.ApplyChanges();

            //Window.IsBorderless = true;

            State = GameState.InMenu;
            Menu = new MainMenu();

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            CurrentInputState = new Input.InputState();

            //THIS LINE DETERMINES FPS
            this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f / 60.0f);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            Sprites.Load(Content);
            UiDrawer.Load(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.F1))
            {
                Exit();
            }

            CurrentInputState.UpdateInput();
            FrameCounter.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            if (State == GameState.InMenu)
            {
                Menu.Update(CurrentInputState);

				if (Menu.ConfirmedSelection == MainMenu.MainMenuChoice.Exit)
				{
					Exit();
				}
				else if (Menu.ConfirmedSelection == MainMenu.MainMenuChoice.Play)
				{
					CurrentLevel = new Level("Content/levels/test1.json");
					CurrentLevel.Load(Content);
					
					State = GameState.InLevel;
				}
				else if (Menu.ConfirmedSelection == MainMenu.MainMenuChoice.Level_Editor)
				{
					// TODO: don't exit here... launch level editor!
					Exit();
				}
            }
            else if(State == GameState.InLevel)
            {
                CurrentLevel.Update(CurrentInputState);
            }
            else if(State == GameState.InLevelEditor)
            {
                //update level editor
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            Resolution.BeginDraw();

            GraphicsDevice.Clear(Color.Black);
            SpriteBatch.Begin(transformMatrix: Resolution.getTransformationMatrix());

            if(State == GameState.InMenu)
            {
                Menu.Draw(SpriteBatch);
            }
            else if (State == GameState.InLevel)
            {
                CurrentLevel.Draw(SpriteBatch);
            }
            else if (State == GameState.InLevelEditor)
            {
                //draw level editor
            }

            SpriteBatch.End();
            base.Draw(gameTime);
        }
    }

    public enum GameState
    {
        InMenu,
        InLevel,
        InLevelEditor
    }
}
