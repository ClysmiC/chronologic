﻿using System;
/// <summary>
/// Credit to "craftworkgames" on StackOverflow!
/// jk, literally copy/pasted and this shit didn't actually work until I fixed it.
/// 
/// Utility class
/// </summary>
using System.Collections.Generic;

namespace Trident.Utility
{
    public static class FrameCounter
    {
        public const int TargetFps = 60;

        public static long TotalFrames { get; private set; }        //total # of frames the game has been running
        public static float TotalSeconds { get; private set; }
        public static float AverageFramesPerSecond { get; private set; }
        public static float CurrentFramesPerSecond { get; private set; }

        public const int MAXIMUM_SAMPLES = 10;
        private static float[] _sampleBuffer = new float[10];
        private static int _cursor = 0;
        private static int counter = 0;


        public static bool Update(float deltaTime)
        {
            CurrentFramesPerSecond = 1.0f / deltaTime;

            _sampleBuffer[_cursor] = CurrentFramesPerSecond;
            _cursor++;
            _cursor %= MAXIMUM_SAMPLES;

            if(counter < 10)
                counter++;

            if (counter == 10)
            {
                AverageFramesPerSecond = 0;

                for (int i = 0; i < MAXIMUM_SAMPLES; i++)
                {
                    AverageFramesPerSecond += _sampleBuffer[i];
                }

                AverageFramesPerSecond /= MAXIMUM_SAMPLES;
            }
            else
            {
                AverageFramesPerSecond = CurrentFramesPerSecond;
            }

            TotalFrames++;
            TotalSeconds += deltaTime;
            return true;
        }
    }
}