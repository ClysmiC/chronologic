﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Utility
{
    class Sprites
    {
        public static Texture2D[] SpritePlayer;
        public static Texture2D ThrowArrowH;
        public static Texture2D ThrowArrowV;

        public static Texture2D GroundTile;
        public static Texture2D OneWayTile;

        public static Texture2D GlassTileHTop;
        public static Texture2D GlassTileHMid;
        public static Texture2D GlassTileHBot;
        public static Texture2D GlassTileVLeft;
        public static Texture2D GlassTileVMid;
        public static Texture2D GlassTileVRight;

        public static Texture2D RampRightTile;
        public static Texture2D HalfRampRightTile1;
        public static Texture2D HalfRampRightTile2;

        public static Texture2D RampLeftTile;
        public static Texture2D HalfRampLeftTile1;
        public static Texture2D HalfRampLeftTile2;

        public static Texture2D ButtonUpTile;
        public static Texture2D ButtonDownTile;

        public static Texture2D Door;

        public static Texture2D Switch;

        public static Texture2D Background;

        public static Texture2D PlayerSelector;
        public static Texture2D PlayerSelectedHighlight;
        public static Texture2D BlackPixel; //can be stretched to draw lines with

        public static Texture2D PulseGrenade;
        public static Texture2D GrenadeBlastRadius;

        public static Texture2D MainMenuBackground;
        public static Texture2D MainMenuItems;

        // TODO: Don't load all sprites on startup. Just load what we need.
        public static void Load(ContentManager contentManager)
        {
            Background = contentManager.Load<Texture2D>("sprites/background");

            SpritePlayer = new Texture2D[4];
            SpritePlayer[0] = contentManager.Load<Texture2D>("sprites/player_1");
            SpritePlayer[1] = contentManager.Load<Texture2D>("sprites/player_2");
            SpritePlayer[2] = contentManager.Load<Texture2D>("sprites/player_3");
            SpritePlayer[3] = contentManager.Load<Texture2D>("sprites/player_4");
            ThrowArrowH = contentManager.Load<Texture2D>("sprites/player_throw_arrow_h");
            ThrowArrowV = contentManager.Load<Texture2D>("sprites/player_throw_arrow_v");

            PulseGrenade = contentManager.Load<Texture2D>("sprites/grenade_strip2");
            GrenadeBlastRadius = contentManager.Load<Texture2D>("sprites/blast_radius");

            PlayerSelector = contentManager.Load<Texture2D>("sprites/player_selector");
            PlayerSelectedHighlight = contentManager.Load<Texture2D>("sprites/player_selected_highlight");

            GroundTile = contentManager.Load<Texture2D>("sprites/tile");
            OneWayTile = contentManager.Load<Texture2D>("sprites/tile_one_way");

            GlassTileHTop = contentManager.Load<Texture2D>("sprites/glass_h_top");
            GlassTileHMid = contentManager.Load<Texture2D>("sprites/glass_h_mid");
            GlassTileHBot = contentManager.Load<Texture2D>("sprites/glass_h_bot");
            GlassTileVLeft = contentManager.Load<Texture2D>("sprites/glass_v_left");
            GlassTileVMid = contentManager.Load<Texture2D>("sprites/glass_v_mid");
            GlassTileVRight = contentManager.Load<Texture2D>("sprites/glass_v_right");

            ButtonUpTile = contentManager.Load<Texture2D>("sprites/button_up");
            ButtonDownTile = contentManager.Load<Texture2D>("sprites/button_down");

            Door = contentManager.Load<Texture2D>("sprites/door");

            Switch = contentManager.Load<Texture2D>("sprites/switch");

            RampRightTile = contentManager.Load<Texture2D>("sprites/tile_ramp_right");
            HalfRampRightTile1 = contentManager.Load<Texture2D>("sprites/tile_half_ramp_right_1");
            HalfRampRightTile2 = contentManager.Load<Texture2D>("sprites/tile_half_ramp_right_2");
            
            RampLeftTile = contentManager.Load<Texture2D>("sprites/tile_ramp_left");
            HalfRampLeftTile1 = contentManager.Load<Texture2D>("sprites/tile_half_ramp_left_1");
            HalfRampLeftTile2 = contentManager.Load<Texture2D>("sprites/tile_half_ramp_left_2");

            BlackPixel = contentManager.Load<Texture2D>("sprites/black_pixel");

            MainMenuBackground = contentManager.Load<Texture2D>("mainmenu/mainmenu_background");
            MainMenuItems = contentManager.Load<Texture2D>("mainmenu/mainmenu_items");
        }
    }
}
