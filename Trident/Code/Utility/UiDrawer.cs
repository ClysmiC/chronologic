﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Utility
{
    static class UiDrawer
    {
        private static SpriteFont font;

        public static void DrawString(SpriteBatch spritebatch, string text, Vector2 position, Color color)
        {
            spritebatch.DrawString(font, text, position, color);
        }

        public static void Load(ContentManager contentManager)
        {
            font = contentManager.Load<SpriteFont>("Arial");
        }

        public static void DrawBlackLine(SpriteBatch spritebatch, Vector2 startPosition, Vector2 endPosition)
        {
            Vector2 edge = endPosition - startPosition;

            float angle = (float)Math.Atan2(edge.Y, edge.X);

            spritebatch.Draw(Sprites.BlackPixel,
            new Rectangle(// rectangle defines shape of line and position of start of line
                (int)startPosition.X,
                (int)startPosition.Y,
                (int)edge.Length(), //sb will strech the texture to fill this rectangle
                1), //width of line, change this to make thicker line
            null,
            Color.Red, //colour of line
            angle,     //angle of line (calulated above)
            new Vector2(0, 0), // point in line about which to rotate
            SpriteEffects.None,
            0);
        }
    }
}
