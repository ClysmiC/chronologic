﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Utility
{
    static class Constants
    {
        //Acceleration due to gravity in normal state
        public const float Gravity = .25f;

        //Time, in milliseconds, that the player may jump without experiencing gravity. Gravity will
        //affect the player when either 1) the player lets go of jump 2) TimeBeforeGravity ms have elapsed
        //3) player hits tile above him, causing him to stop
        public const int FramesBeforeGravityJump = 15;

        //when strong thrown horizontally, you go without gravity or control for a bit, to get some true horizontal action!
        public const int FramesBeforeControlThrownStrongH = 20;

        //amount of frames that throwing lasts for
        public const int ThrowDurationFrames = 20;

        //speed reached when thrown-- should be higher than max speed!
        public const float ThrowSpeedH = 10f;
        public const float ThrowSpeedLift = 8f; // upwards "lift" on horizontal weak throws
        public const float ThrowSpeedV = 10f;

        public const float ThrowSpeedStrongH = 15;
        public const float ThrowSpeedStrongV = 13;

        public const int ThrowCooldown = 5; //frames

       

        /************PLAYER MOVEMENT*****************/
        /*
         * The following variables are constants for the player's movement. Many of them have three values:
         * 1. The value that is used when the player is not holding the sprint button.
         * 2. The value that is used when the player is holding the sprint button (except PlayerFast)
         * 3. The value that is used when PlayerFast is holding the sprint button.         * 
         */

        //Rate of acceleration for horizontal movement in air and ground. For decelerating in the air,        
        //see PlayerReverseAirAcceleration
        public const float PlayerAcceleration = .1f;
        public const float PlayerRunAcceleration = .2f;
        public const float PlayerFastRunAcceleration = .3f;


        //Rate of deceleration that passively happens when traveling through the air. Only applied when
        //stick is held neutral when in air. If stick is held forward, normal acceleration applies. If
        //stick is held backwards, normal acceleration applies in the backwards direction
        public const float PlayerAirFriction = .15f;
        public const float PlayerRunAirFriction = .1f;

        //Rate of deceleration that passively happens when travelling on the ground.
        //THIS FRICTION IS ONLY APPLIED IF THE SPEED IS GREATER THAN PlayerMaxSpeed!!!
        public const float PlayerGroundFriction = .15f;
        public const float PlayerRunGroundFriction = .05f;

        //Rate of deceleration when holding joystick AGAINST the direction of movement in the air.
        //Should be higher than PlayerAcceleration or else player will feel like they have litte
        //control in the air.
        public const float PlayerReverseAirAcceleration = .5f;
        public const float PlayerRunReverseAirAcceleration = .7f;
        public const float PlayerFastRunReverseAirAcceleration = .7f;

        //Maximum speed that player can run at, without the help of another force, such as being
        //thrown or propelled by a bomb. While bombs/throws can cause this speed to be exceeded,
        //the player will slowly decelerate back to this speed after a short while.
        public const float PlayerMaxSpeed = 3f;
        public const float PlayerRunMaxSpeed = 5f;
        public const float PlayerFastRunMaxSpeed = 7f;

        //Maximum falling speed. Currently not enforced, might be worth deleting.
        public const float PlayerMaxFallSpeed = 12f;

        public const float GrenadeThrowSpeedH = 6f;
        public const float GrenadeThrowSpeedV = 8f;
        public const float GrenadeThrowLift = 4f; // when thrown horizontal, give it some "lift" so it doesn't just plop
        public const float GrenadeStrongThrowSpeedH = 12f;
        public const float GrenadeStrongThrowSpeedV = 12f;
        public const float GrenadeGroundInitialFriction = .5f;  //friction when first hitting the ground or bouncing
        public const float GrenadeGroundFriction = .3f;         //friction when sliding on the ground

        public const int GrenadeTimer = 180; //frames
        public const float GrenadeBlastMaxStrength = 20f;
        public const float GrenadeBlastMinStrength = 5f;
        public const float GrenadeBlastRadius = 256f;

        //the speed that an item must be going to break a pane of glass.
        //should be higher than the sprint speed but lower than the throw speed.
        public const float GlassBreakSpeed = 10f;

        //Vertical speed that player acheives at the very beginning of the jump. Will be changed
        //once hold-to-jump-higher features are added.
        public const float PlayerJumpSpeed = 3.5f;
    }
}
