﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.Code.Utility
{
    class CollisionDetection
    {
        public static bool Intersects(Vector2[] hitboxCoordinates, Vector2[] hitbox2Coordinates)
        {
            if (hitboxCoordinates.Length < 3)
            {
                return false;
            }

            //A = tile
            //B = rectangular hitbox
            int edgeCountA = hitboxCoordinates.Length;
            int edgeCountB = hitbox2Coordinates.Length;

            Vector2 startPoint;
            Vector2 endPoint;
            Vector2 edge;

            // Loop through all the edges of both polygons
            for (int i = 0; i < edgeCountA + edgeCountB; i++)
            {
                if (i < edgeCountA)
                {
                    startPoint = hitboxCoordinates[i];
                    endPoint = hitboxCoordinates[(i + 1) % edgeCountA];
                    edge = endPoint - startPoint;
                }
                else
                {
                    startPoint = hitbox2Coordinates[i - edgeCountA];
                    endPoint = hitbox2Coordinates[(i - edgeCountA + 1) % edgeCountB];
                    edge = endPoint - startPoint;
                }

                //edge rotated 90 degrees
                Vector2 normalAxis = new Vector2(-edge.Y, edge.X);
                normalAxis.Normalize();

                float minA, maxA, minB, maxB;
                ProjectPolygon(normalAxis, hitboxCoordinates, out minA, out maxA);
                ProjectPolygon(normalAxis, hitbox2Coordinates, out minB, out maxB);

                //if projections don't overlap
                if (!(minA == minB || maxA == maxB || minA == maxB || maxA == minB || (minA < minB && maxA > minB) || (minB < minA && maxB > minA)))
                {
                    return false;
                }
            }

            return true;
        }

        public static void ProjectPolygon(Vector2 axis, Vector2[] polygon, out float min, out float max)
        {
            // To project a point on an axis use the dot product
            float dotProduct = Vector2.Dot(axis, polygon[0]);
            min = dotProduct;
            max = dotProduct;

            for (int i = 0; i < polygon.Length; i++)
            {
                dotProduct = Vector2.Dot(axis, polygon[i]);

                if (dotProduct < min)
                {
                    min = dotProduct;
                }
                else
                {
                    if (dotProduct > max)
                    {
                        max = dotProduct;
                    }
                }
            }
        }
    }
}
