﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trident.DataObjects
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        NoDirection
    }
}
