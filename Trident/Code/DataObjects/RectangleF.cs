﻿using Microsoft.Xna.Framework;
namespace Trident.DataObjects
{
    //why is this not built in to XNA/Monogame???
    public class RectangleF
    {
        private float _x;
        public float X
        {
            get { return _x; }
            set
            {
                _x = value;
                calculateCoordinates();
            }
        }

        private float _y;
        public float Y
        {
            get { return _y; }
            set
            {
                _y = value;
                calculateCoordinates();
            }
        }

        private float _width;
        public float Width
        {
            get { return _width; }
            set
            {
                _width = value;
                calculateCoordinates();
            }
        }

        private float _height;
        public float Height
        {
            get { return _height; }
            set
            {
                _height = value;
                calculateCoordinates();
            }
        }

        public Vector2[] Coordinates { get; }

        public RectangleF(float x, float y, float width, float height)
        {
            _x = x; _y = y; _width = width; _height = height;
            Coordinates = new Vector2[4];
            calculateCoordinates();
        }

        public void calculateCoordinates()
        {
            Coordinates[0] = new Vector2(_x, _y);
            Coordinates[1] = new Vector2(_x + _width - 1, _y);
            Coordinates[2] = new Vector2(_x + _width - 1, _y + _height - 1);
            Coordinates[3] = new Vector2(_x, _y + _height - 1);
        }
    }
}