﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.DataObjects;
using Trident.Environment;
using Trident.Utility;
using Trident.Input;

namespace Trident.Controllable
{
    public class PlayerGrenade : Player
    {
        public PulseGrenade Grenade { get; set; }

        public PlayerGrenade(Level level, Vector2 startingPosition)
            : base(level, startingPosition, 1)
        {
        }

        public override void HandleSpecialInputs(InputState input)
        {
            if (input.isPressed(input.special))
            {
                if (Grenade == null)
                {
                    Grenade = new PulseGrenade(positionOfCenter().X, positionOfCenter().Y, this);
                    Level.ActiveGrenade = Grenade;
                }
                else
                {
                    Grenade.Explode();
                }
            }
        }

        public override void UpdateSpecialItems()
        {
            if(Grenade != null)
            {
                Grenade.Update();

                if (Grenade.Exploded)
                {
                    Grenade = null;
                }
            }
        }

        public override void ShiftSpecialItemsDueToMovingPlatform(Vector2 shiftVector)
        {
            if(Grenade != null && !Grenade.Thrown)
            {
                Grenade.ShiftDueToMovingPlatform(shiftVector);
            }
        }

        public override void SpecialItemsActionOnDeath()
        {
            if (Grenade != null && !Grenade.Thrown)
            {
                Grenade.Explode();
            }
        }

        public override void ReinitializeSpecialItems()
        {
            Grenade = null;
        }

        public override void DrawSpecialItems(SpriteBatch spriteBatch, GameCamera camera)
        {
            if (Grenade != null)
            {
                Grenade.Draw(spriteBatch, camera);
            }
        }

        public override void ThrowItem(Direction direction, InputState input)
        {
            if (Grenade != null && !Grenade.Thrown)
            {
                ThrowGrenade(direction, input);
            }
            else
            {
                base.ThrowItem(direction, input);
            }
        }

        public void ThrowGrenade(Direction direction, InputState input)
        {
            if(!input.sprint.isDown)
            {
                Grenade.GetThrownWeak(direction);
            }
            else
            {
                Grenade.GetThrownStrong(direction);
            }

            //don't perform additional throws
            throwingDirection = Direction.NoDirection;
        }
    }







    public class PulseGrenade : Movable
    {
        //0: normal grenade
        //1: red flash
        protected int _imageIndex;

        private PlayerGrenade thrower;
        
        public bool Thrown { get; set; }
        public int Timer { get; set; }

        //set true when the grenade explodes. PlayerGrenade and Level will check for this variable each frame,
        //and set their references to null if it has exploded, letting the GC do it's thing.
        public bool Exploded { get; set; }

        public PulseGrenade(float x, float y, PlayerGrenade player)
        {
            const int width = 10;
            const int height = 10;

            Thrown = false;
            IsOnGround = false;

            Timer = Constants.GrenadeTimer;

            XSpeed = 0;
            YSpeed = 0;
            thrower = player;
            Level = player.Level;

            Hitbox = new RectangleF(x - width/2, y - height/2, width, height);
            _imageIndex = 0;
        }

        public void Update()
        {
            //explode if crushed
            if (!Level.IsPositionFreeForMovableItem(Hitbox, this))
            {
                Explode();
            }

            if (Timer > 0) { Timer--; }

            if(Timer == 150 || Timer == 90 || Timer == 30)
            {
                _imageIndex = 1;
            }
            else if(Timer == 140 || Timer == 80 || Timer == 20)
            {
                _imageIndex = 0;
            }

            if(Timer <= 0)
            {
                Explode();
            }

            if(!Thrown)
            {
                Hitbox.X = thrower.positionOfCenter().X - Hitbox.Width / 2;
                Hitbox.Y = thrower.positionOfCenter().Y - Hitbox.Height / 2;
            }
            else if(!Exploded)
            { 
                //update if on ground or not
                if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y + 1, Hitbox.Width, Hitbox.Height), this))
                {
                    IsOnGround = false;
                }
                else
                {
                    IsOnGround = true;
                }

                //update y speed due to gravity
                if (!IsOnGround)
                {
                    YSpeed += Constants.Gravity;
                }

                //slow grenade down if sliding on ground
                if (XSpeed != 0 && IsOnGround)
                {
                    XSpeed += (XSpeed > 0) ? -Constants.GrenadeGroundFriction : Constants.GrenadeGroundFriction;

                    if (Math.Abs(XSpeed) < 1)
                    {
                        XSpeed = 0;
                    }
                }

                //update x position
                if(thrower.Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X + XSpeed, Hitbox.Y, Hitbox.Width, Hitbox.Height), this))
                {
                    Hitbox.X += XSpeed;
                }
                else if (XSpeed != 0)
                {
                    MoveToContact((XSpeed > 0) ? Direction.Right : Direction.Left, XSpeed);

                    //rebound
                    XSpeed = -(XSpeed / 2);
                }

                //update y position
                if (thrower.Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y + YSpeed, Hitbox.Width, Hitbox.Height), this))
                {
                    Hitbox.Y += YSpeed;
                }
                else if (YSpeed != 0)
                {
                    MoveToContact((YSpeed > 0) ? Direction.Down : Direction.Up, YSpeed);

                    //rebound
                    YSpeed = -(YSpeed / 2);

                    //friction on rebound
                    if (XSpeed != 0)
                    {
                        XSpeed += (XSpeed > 0) ? Constants.GrenadeGroundInitialFriction : -Constants.GrenadeGroundInitialFriction;
                    }

                    //settle after bouncing low
                    if(Math.Abs(YSpeed) < 1)
                    {
                        YSpeed = 0;
                    }
                }
            }
        }

        public override void ShiftDueToMovingPlatform(Vector2 shiftVector)
        {
            Hitbox.X += shiftVector.X;
            Hitbox.Y += shiftVector.Y;
        }

        public override void GetThrownWeak(Direction direction)
        {
            Thrown = true;

            XSpeed = (direction == Direction.Left) ? -Constants.GrenadeThrowSpeedH : (direction == Direction.Right) ? Constants.GrenadeThrowSpeedH : 0;
            YSpeed = (direction == Direction.Up) ? -Constants.GrenadeThrowSpeedV : (direction == Direction.Down) ? Constants.GrenadeThrowSpeedV : (direction == Direction.Right || direction == Direction.Left) ? -Constants.GrenadeThrowLift : 0;
        }

        public override void GetThrownStrong(Direction direction)
        {
            Thrown = true;

            XSpeed = (direction == Direction.Left) ? -Constants.GrenadeStrongThrowSpeedH : (direction == Direction.Right) ? Constants.GrenadeStrongThrowSpeedH : 0;
            YSpeed = (direction == Direction.Up) ? -Constants.GrenadeStrongThrowSpeedV : (direction == Direction.Down) ? Constants.GrenadeStrongThrowSpeedV : (direction == Direction.Right || direction == Direction.Left) ? -Constants.GrenadeThrowLift : 0;
        }

        /// <summary>
        /// Explodes the grenade, and propels nearby players to high speeds!
        /// </summary>
        public void Explode()
        {
            if (Exploded) { return; }


            Timer = 0;
            Exploded = true;

            if (!Thrown)
            {
                //at the moment, holding on to the grenade just blasts you straight up.
                //if I want to kill the grenadier if they don't let go, do it here
            }

            foreach(Player player in Level.PlayerArray)
            {
                if (!player.UsedInLevel && !player.IsActivePlayer) { continue; }

                Vector2 toPlayer = player.positionOfCenter() - this.positionOfCenter();

                if(Math.Abs(toPlayer.Length()) <= Constants.GrenadeBlastRadius)
                {
                    float lerpPercent = MathHelper.Clamp(Math.Abs(toPlayer.Length()) / Constants.GrenadeBlastRadius, 0, 1);
                    float forceOnPlayer = MathHelper.Lerp(Constants.GrenadeBlastMaxStrength, Constants.GrenadeBlastMinStrength, lerpPercent);

                    if(toPlayer.X == 0 && toPlayer.Y == 0)
                    {
                        //if zero vector, just blast straight up
                        //this step is necessary, because trying to normalize a 0-vector results in NaNs
                        toPlayer = new Vector2(0, -1);
                    }

                    toPlayer.Normalize();
                    toPlayer = toPlayer * forceOnPlayer;

                    player.XSpeed += toPlayer.X;
                    player.YSpeed += toPlayer.Y;
                }
            }
        }

        public void Draw(SpriteBatch sb, GameCamera camera)
        {
            
            Vector2 cameraTopLeft = new Vector2(camera.Position.X - camera.Width / 2, camera.Position.Y - camera.Height / 2);

            Texture2D blast = Sprites.GrenadeBlastRadius;

            //TODO: draw this on layer on top of players
            sb.Draw(Sprites.PulseGrenade, new Vector2(Hitbox.X, Hitbox.Y) - cameraTopLeft, new Rectangle(_imageIndex * (int)Hitbox.Width, 0, (int)Hitbox.Width, (int)Hitbox.Height), Color.White);

            //TODO: only draw this when exploding, making it look better
            sb.Draw(blast, positionOfCenter() - new Vector2(blast.Width / 2, blast.Height / 2) - cameraTopLeft, Color.White);
        }
    }
}
