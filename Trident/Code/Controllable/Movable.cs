﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.DataObjects;
using Trident.Environment;

namespace Trident.Controllable
{
    public abstract class Movable
    {
        public RectangleF Hitbox { get; set; }
        public float XSpeed { get; set; }
        public float YSpeed { get; set; }
        public bool IsOnGround { get; set; }

        public Level Level { get; protected set; }

        //refactor this somehow. I have (mostly) the same implementation for Player and PulseGrenade
        public abstract void ShiftDueToMovingPlatform(Vector2 shiftVector);
        public abstract void GetThrownStrong(Direction direction);
        public abstract void GetThrownWeak(Direction direction);

        public virtual void MoveToContact(Direction direction, float maximum)
        {
            maximum = Math.Abs(maximum);

            if (direction == Direction.Right)
            {
                //go from largest to smallest movements so that it can exit early when
                //a movement is found
                for (int i = (int)Math.Round(maximum); i >= 1; i--)
                {

                    if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X + i, Hitbox.Y, Hitbox.Width, Hitbox.Height), this))
                    {
                        Hitbox.X += i;
                        break;
                    }
                }

                XSpeed = 0;
            }
            else if (direction == Direction.Left)
            {
                for (int i = (int)Math.Round(maximum); i >= 1; i--)
                {
                    if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X - i, Hitbox.Y, Hitbox.Width, Hitbox.Height), this))
                    {
                        Hitbox.X -= i;
                        break;
                    }
                }

                XSpeed = 0;
            }
            else if (direction == Direction.Up)
            {
                for (int i = (int)Math.Round(maximum); i >= 1; i--)
                {
                    if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y - i, Hitbox.Width, Hitbox.Height), this))
                    {
                        Hitbox.Y -= i;
                        break;
                    }
                }

                YSpeed = 0;
            }
            else if (direction == Direction.Down)
            {
                for (int i = (int)Math.Round(maximum); i >= 1; i--)
                {
                    if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y + i, Hitbox.Width, Hitbox.Height), this))
                    {
                        Hitbox.Y += i;
                        break;
                    }
                }

                YSpeed = 0;
            }
        }

        public virtual Vector2 positionOfCenter()
        {
            return new Vector2(Hitbox.X + Hitbox.Width / 2, Hitbox.Y + Hitbox.Height / 2);
        }
    }
}
