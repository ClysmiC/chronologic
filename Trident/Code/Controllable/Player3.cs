﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Environment;

namespace Trident.Controllable
{
    class Player3 : Player
    {
        public Player3(Level level, Vector2 startingPosition)
            : base(level, startingPosition, 3)
        {
        }
    }
}