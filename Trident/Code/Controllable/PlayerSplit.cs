﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Environment;
using Trident.Input;

namespace Trident.Controllable
{
    class PlayerSplit : Player
    {
        public PlayerSplit(Level level, Vector2 startingPosition)
            : base(level, startingPosition, 2)
        {
        }

        public override void HandleSpecialInputs(InputState input)
        {
        }

        public override void UpdateSpecialItems()
        {
        }

        public override void DrawSpecialItems(SpriteBatch spriteBatch, GameCamera camera)
        {
        }
    }
}
