﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;
using Trident.Environment;
using Trident.Environment.EnvironmentObjects;
using Trident.DataObjects;
using Trident.Utility;
using Trident.Input;

namespace Trident.Controllable
{
    //TODO: level should be field of player
    public abstract class Player : Movable
    {
        public int Id { get; set; } 
        public bool IsActivePlayer { get; set; }
        public bool UsedInLevel { get; set; } //starts false until the user actually uses the character for the entirety of a level

        protected long frameLeftGround; //used to let you jump a few frames after dropping off the ground

        protected Texture2D currentSprite;
        protected float currentAnimationFrame;
        protected int numAnimationFrames;
        protected float animationSpeed = .3f;
        protected Vector2 startingPosition;

        protected InputState[] inputRecord;
        protected int inputRecordIndex; //used to index into the inputRecord

        //determines whether to use normal or reduced gravity, to acheive higher jumps while the jump button is held.
        protected bool jumpActive;

        //stores the direction that the player is currenty trying to throw.
        //if the palyer isn't throwing, the value is NoDirection
        protected Direction throwingDirection;

        //whether or not the player's position was pushed up to go up a ramp
        //if true, need to snap back down to ground at the end of the frame
        protected bool rampAdjusted;

        //used to determine when gravity should start being applied
        protected long frameOfJump;
        protected long frameThrown;  //throwee
        protected long frameThrowing; //thrower
        protected long frameDoneThrowing; //used for cooldown

		public bool ThrownActiveH()
		{
			return (FrameCounter.TotalFrames - frameThrown <= Constants.FramesBeforeControlThrownStrongH);
		}

   
        public Player(Level level, Vector2 position, int id)
        {
            Id = id;
            currentSprite = Sprites.SpritePlayer[Id];
            currentAnimationFrame = 0;
            numAnimationFrames = 8;

            startingPosition = position;

            Hitbox = new RectangleF(0, 0, 24, 36);

            Level = level;
            Hitbox.X = startingPosition.X;
            Hitbox.Y = startingPosition.Y;

            IsActivePlayer = false;
            UsedInLevel = false;

            jumpActive = false;
            throwingDirection = Direction.NoDirection;
            rampAdjusted = false;

            //hasn't jumped or been thrown yet, so just set to some arbitrary
            //negative value so it doesn't have strange behavior near the start of
            //the game (frame 0)
            frameOfJump = -1000;
            frameThrown = -1000;
            frameThrowing = -1000;
            frameDoneThrowing = -1000;
            frameLeftGround = -1000;

            inputRecordIndex = 0;
            inputRecord = new InputState[Level.MaxFramesForLevel];
        }

        /// <summary>
        /// Upon the ending of one iteration and the beginning of the next, all players need to be reinitialized
        /// </summary>
        public void Reinitialize()
        {
            Hitbox.X = startingPosition.X;
            Hitbox.Y = startingPosition.Y;
            XSpeed = 0;
            YSpeed = 0;
            jumpActive = false;
            IsActivePlayer = false;
            inputRecordIndex = 0;

            frameOfJump = -1000;
            frameThrown = -1000;
            frameThrowing = -1000;
            frameDoneThrowing = -1000;
            frameLeftGround = -1000;

            ReinitializeSpecialItems();
        }

        public virtual void ReinitializeSpecialItems() {}

        public void Load(ContentManager contentManager)
        {
        }

        public virtual void HandleSpecialInputs(InputState input) {}
        public virtual void UpdateSpecialItems() {}                     //e.g., move grenade with the PlayerGrenade
        public virtual void ShiftSpecialItemsDueToMovingPlatform(Vector2 shiftVector) { }   //e.g., shift grenade with PlayerGrenade

        public virtual void SpecialItemsActionOnDeath() { }

        public virtual void SetVariablesBasedOnInputs(InputState input, out float xAcceleration, out float xMaxSpeed,
            out float xReverseAirAcceleration, out float xAirFriction, out float xGroundFriction, out float xMaxRunSpeed)
        {
            xAcceleration =
                (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerRunAcceleration : Constants.PlayerAcceleration;
            
            xMaxSpeed =
                (input.sprint.isDown) ? Constants.PlayerRunMaxSpeed : Constants.PlayerMaxSpeed;

            xReverseAirAcceleration
                = (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerRunReverseAirAcceleration : Constants.PlayerReverseAirAcceleration;

            xAirFriction
                = (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerRunAirFriction : Constants.PlayerAirFriction;

            xGroundFriction =
                (input.sprint.isDown) ? Constants.PlayerRunGroundFriction : Constants.PlayerGroundFriction;

            xMaxRunSpeed = Constants.PlayerRunMaxSpeed;
        }

        public void Update(InputState thisFramesInput)
        {
            if(UsedInLevel == false && IsActivePlayer == false)
            {
                return;
            }


            Rectangle gameView = Level.Bounds;

            if (!Level.IsPositionFreeForMovableItem(Hitbox, this))
            {
                //temporary way to "kill" player
                SpecialItemsActionOnDeath();

                Hitbox.X = 0;
                Hitbox.Y = 0;
            }

            InputState input;

            if (IsActivePlayer)
            {
                input = thisFramesInput;
            }
            else
            {
                input = inputRecord[Level.CurrentFrame];
            }


            //all horizontal movement uses these variables. Initialize them to the correct value depending
            //on whether sprint is pressed
            float xAcceleration;
            float xMaxSpeed;
            float xReverseAirAcceleration;
            float xAirFriction;
            float xGroundFriction;
            float xMaxRunSpeed; //needed even if we aren't running, b/c we could be > max speed due to throw or grenade

            SetVariablesBasedOnInputs(input, out xAcceleration, out xMaxSpeed, out xReverseAirAcceleration, out xAirFriction, out xGroundFriction, out xMaxRunSpeed);

            //update IsOnGround
            if (Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y + 1, Hitbox.Width, Hitbox.Height), this))
            {
                if(IsOnGround)
                {
                    frameLeftGround = FrameCounter.TotalFrames;
                }

                IsOnGround = false;
            }
            else
            {
                IsOnGround = true;
                jumpActive = false;
            }

            //handle X inputs, which only some characters receive
            HandleSpecialInputs(input);

            //throw!
            if(throwingDirection == Direction.NoDirection && FrameCounter.TotalFrames - frameDoneThrowing >= Constants.ThrowCooldown)
            {

                if (input.isPressed(input.throwRight))
                {
                    frameThrowing = FrameCounter.TotalFrames;
                    throwingDirection = Direction.Right;
                }
                else if (input.isPressed(input.throwUp))
                {
                    frameThrowing = FrameCounter.TotalFrames;
                    throwingDirection = Direction.Up;
                }
                else if (input.isPressed(input.throwLeft))
                {
                    frameThrowing = FrameCounter.TotalFrames;   
                    throwingDirection = Direction.Left;
                }
                else if (input.isPressed(input.throwDown) && !IsOnGround)
                {
                    frameThrowing = FrameCounter.TotalFrames;  
                    throwingDirection = Direction.Down;
                }
            }
            
            //stop throwing after 1 second
            if (throwingDirection != Direction.NoDirection && FrameCounter.TotalFrames - frameThrowing >= Constants.ThrowDurationFrames)
            {
                frameDoneThrowing = FrameCounter.TotalFrames;
                throwingDirection = Direction.NoDirection;
            }

            //attempt to do the throw
            if(throwingDirection != Direction.NoDirection)
            {
                //throwingDirection gets updated (if the throw happens) in the ThrowItem code
                ThrowItem(throwingDirection, input);
            }

            //handle interact button
            if (input.isPressed(input.interact))
            {
                Level.ToggleSwitchWithinRegion(Hitbox);
            }

            //handle right/left
            if (input.right.isDown && !ThrownActiveH())
            {
                //reverse acceleration if in air
                if(!IsOnGround && XSpeed < 0)
                {
                    XSpeed += xReverseAirAcceleration;
                }
                //decelerate (but don't stop on dime) if max speed in oppo direction is > PlayerMaxSpeed
                else if (IsOnGround && XSpeed < -xMaxRunSpeed)
                {
                    //neutral stick results in 2 * friction, so this must be even greater
                    XSpeed += (xAcceleration + 2 * xGroundFriction);
                }
                //otherwise, stop on a dime when inputting against the direction of travel
                else if (IsOnGround && XSpeed < 0)
                {
                    XSpeed = 0;
                }
                //normal acceleration if on ground or accelerating forward in the air
                //unless XSpeed is > MaxSpeed
                else if (XSpeed >= 0 && XSpeed < xMaxSpeed)
                {
                    XSpeed = Math.Min(XSpeed + xAcceleration, xMaxSpeed);
                }
                //slow down due to friction if over the max speed
                else if(XSpeed >= xMaxSpeed)
                {
                    XSpeed -= xGroundFriction;
                }
            }
            else if (input.left.isDown && !ThrownActiveH())
            {
                //reverse acceleration is greater when in air
                if (!IsOnGround && XSpeed > 0)
                {
                    XSpeed -= xReverseAirAcceleration;
                }
                //decelerate (but don't stop on dime) if max speed in oppo direction is > PlayerMaxSpeed
                else if(IsOnGround && XSpeed > xMaxRunSpeed)
                {
                    //neutral stick results in 2 * friction, so this must be even greater
                    XSpeed -= (xAcceleration + 2 * xGroundFriction);
                }
                //otherwise stop on a dime when inputting against the direction of travel (UNLESS GOING > PlayerMaxSpeed)
                else if (IsOnGround && XSpeed > 0)
                {
                    XSpeed = 0;
                }
                //normal acceleration if on ground or accelerating forward in the air
                //unless XSpeed is > MaxSpeed
                else if (XSpeed <= 0 && XSpeed > -xMaxSpeed)
                {
                    XSpeed = MathHelper.Max(XSpeed - xAcceleration, -xMaxSpeed);
                }
                //slow down due to friction if over the max speed
                else if(XSpeed <= -xMaxSpeed)
                {
                    XSpeed += xGroundFriction;
                }
            }
            //stop on a dime on the ground if you are neutral (if not going faster than a run)
            else if (IsOnGround && Math.Abs(XSpeed) <= xMaxRunSpeed)
            {
                XSpeed = 0;
            }
            //slow down due to ground friction if you are neutral and going too fast
            else if (IsOnGround)
            {
                //friction is twice as strong since the player isn't trying to maintain speed
                //(i.e., they have gone neutral)
                if (XSpeed > xMaxRunSpeed)
                {
                    XSpeed -= 2 * xGroundFriction;
                }

                if (XSpeed < -xMaxRunSpeed)
                {
                    XSpeed += 2 * xGroundFriction;
                }
            }
            //air friction when stick is neutral
            else if(!IsOnGround && !ThrownActiveH())
            {
                //air friction!
                if (XSpeed > 0)
                {
                    XSpeed = Math.Max(0, XSpeed - xAirFriction);
                }
                else if (XSpeed < 0)
                {
                    XSpeed = Math.Min(0, XSpeed + xAirFriction);
                }
            }

            //handle jump
            if (input.isPressed(input.jump))
            {
                if(IsOnGround || FrameCounter.TotalFrames - frameLeftGround < 3)
                {
                    YSpeed = -Constants.PlayerJumpSpeed;
                    jumpActive = true;
                    frameOfJump = FrameCounter.TotalFrames;
                }
            }

            //fall due to gravity!
            if(!IsOnGround)
            {
                //let go of jump button or at jump peak?
                //gravity increases to normal strength to make shorter taps = shorter jumps
                if(!input.jump.isDown || YSpeed >= 0 || (FrameCounter.TotalFrames - frameOfJump) > Constants.FramesBeforeGravityJump)
                {
                    jumpActive = false;
                }

                //TODO: consider clamping with Constants.PlayerMaxFallSpeed
                YSpeed += (jumpActive) ? 0 : (ThrownActiveH()) ? 0 : Constants.Gravity;
            }

            const float rampAdjust = 1.4f;

            //update x position
            if (XSpeed != 0 && Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X + XSpeed, Hitbox.Y, Hitbox.Width, Hitbox.Height), this))
            {
                Hitbox.X += XSpeed;
            }
            //if can't move directly, check if player can move up a slope. rampAdjust must be greater than 1
            //to go up 1:1 ramps successfully (since the slope of the ramp is 1). 1.4 was chosen as kind of a
            //"magic" value, but it works.
            else if (XSpeed != 0 && IsOnGround && Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X + XSpeed, Hitbox.Y - Math.Abs(rampAdjust * XSpeed), Hitbox.Width, Hitbox.Height), this))
            {
                Hitbox.X += XSpeed;
                Hitbox.Y -= Math.Abs(rampAdjust * XSpeed);
                rampAdjusted = true;
            }
            else if (XSpeed != 0)
            {
                MoveToContact((XSpeed > 0) ? Direction.Right : Direction.Left, XSpeed);
                frameThrown = -1000;
            }

            //update y position if possible
            if(YSpeed != 0 && Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X, Hitbox.Y + YSpeed, Hitbox.Width, Hitbox.Height), this))
            {
                Hitbox.Y += YSpeed;
            }
            else if(YSpeed != 0) {
                MoveToContact((YSpeed > 0) ? Direction.Down : Direction.Up, YSpeed);
            }
            //else YSpeed must = 0 so do nothing!


            const float distanceToCheckBelowForRamp = 16.0f;
            if(rampAdjusted)
            {
                if(IsOnGround && !jumpActive)
                {
                    //20 is totally a magic number. it could probably be less, but 20 seems to work
                    //fyi, it is the amount to snap the player back down after geting shifted up when
                    //ascending a ramp
                    MoveToContact(Direction.Down, 20);
                    rampAdjusted = false;
                }
            }
                //check if ramp is directly below, and snap down to the ramp (lets player smoothly go down ramps)
                //make sure YSpeed is very small, because that means we aren't coincidentally landing on a slope
                //make sure XSpeed is <= xMaxRunSpeed so that thrown players won't go down ramps
            else if (!jumpActive && YSpeed == 0 && Math.Abs(XSpeed) <= xMaxRunSpeed &&
                (Level.IsRampBelow(Hitbox.X, Hitbox.Y + Hitbox.Height - 1, distanceToCheckBelowForRamp) ||
                Level.IsRampBelow(Hitbox.X + Hitbox.Width - 1, Hitbox.Y + Hitbox.Height - 1, distanceToCheckBelowForRamp)))
            {
                MoveToContact(Direction.Down, distanceToCheckBelowForRamp);
            }

            currentAnimationFrame = (currentAnimationFrame + animationSpeed) % numAnimationFrames;

            UpdateSpecialItems();

            //save a snapshot of the inputstate if it is an active player
            if (IsActivePlayer)
            {
                inputRecord[inputRecordIndex] = new InputState(input);
                inputRecordIndex++;
            }
            else
            {
                //TODO: check for desync
            }
        }

        public override void ShiftDueToMovingPlatform(Vector2 shiftVector)
        {
            if(Level.IsPositionFreeForMovableItem(new RectangleF(Hitbox.X + shiftVector.X, Hitbox.Y + shiftVector.Y, Hitbox.Width, Hitbox.Height), this))
            {
                Hitbox.X += shiftVector.X;
                Hitbox.Y += shiftVector.Y;

                ShiftSpecialItemsDueToMovingPlatform(shiftVector);
            }
        }

        public virtual void ThrowItem(Direction direction, InputState input)
        {
            Player[] players = Level.PlayerArray;

            for(int i = 0; i < Level.MaxPlayerCopyCount; i++)
            {
                int index = Level.PlayerIdChronological[i];

                Movable item;

                //don't throw yourself!
                if (index == Id)
                {
                    //for the iteration that checks against yourself,
                    //use check against the active grenade instead
                    if (Level.ActiveGrenade == null || Level.ActiveGrenade.Thrown == false)
                    {
                        continue;
                    }
                    else
                    {
                        item = Level.ActiveGrenade;
                    }
                }
                else
                {
                    item = Level.PlayerArray[index];
                }


                //don't throw players that haven't been used yet
                if(item is Player && ((Player)item).UsedInLevel == false && ((Player)item).IsActivePlayer == false)
                {
                    continue;
                }

                if (IsItemInsideHitbox(item))
                {
                    //snap Hitbox to a fixed position relative to thrower
                    item.Hitbox.X = this.Hitbox.X;
                    item.Hitbox.Y = this.Hitbox.Y;


                    //is it inconsistent to set the throwee's x/y a few lines above,
                    //then call a method to set the rest of the throwee's variables?
                    if (input.sprint.isDown)
                    {
                        item.GetThrownStrong(direction);
                    }
                    else
                    {
                        item.GetThrownWeak(direction);
                    }


                    //don't perform additional throws
                    throwingDirection = Direction.NoDirection;
                    frameDoneThrowing = FrameCounter.TotalFrames;

                    return; //only throw one player!
                }
            }
        }

        public override void GetThrownWeak(Direction direction)
        {
            XSpeed = (direction == Direction.Right) ? Constants.ThrowSpeedH : (direction == Direction.Left) ? -Constants.ThrowSpeedH : 0;
            YSpeed = (direction == Direction.Down) ? Constants.ThrowSpeedV : (direction == Direction.Up) ? -Constants.ThrowSpeedV : -Constants.ThrowSpeedLift;

            //can't throw immediately after being thrown to prevent "Luigi ladder" style throwing chains
            frameDoneThrowing = FrameCounter.TotalFrames;

            frameOfJump = 0; //necessary? idk, but don't let them do tricky stuff to extend their time without gravity
        }

        public override void GetThrownStrong(Direction direction)
        {
            XSpeed = (direction == Direction.Right) ? Constants.ThrowSpeedStrongH : (direction == Direction.Left) ? -Constants.ThrowSpeedStrongH : 0;
            YSpeed = (direction == Direction.Down) ? Constants.ThrowSpeedStrongV : (direction == Direction.Up) ? -Constants.ThrowSpeedStrongV : 0;

            //only defy gravity when thrown strong horizontally
            if (direction == Direction.Right || direction == Direction.Left)
            {
                frameThrown = FrameCounter.TotalFrames;
            }

            //can't throw immediately after being thrown to prevent "Luigi ladder" style throwing chains
            frameDoneThrowing = FrameCounter.TotalFrames;

            frameOfJump = 0; //necessary? idk, but don't let them do tricky stuff to extend their time without gravity
        }

        public bool IsPointInsideHitbox(Vector2 point)
        {
            return point.X >= Hitbox.X && point.X <= Hitbox.X + Hitbox.Width - 1 && point.Y >= Hitbox.Y && point.Y <= Hitbox.Y + Hitbox.Height - 1;
        }

        public bool IsItemInsideHitbox(Movable item)
        {
            return IsPointInsideHitbox(new Vector2(item.Hitbox.X, item.Hitbox.Y)) || IsPointInsideHitbox(new Vector2(item.Hitbox.X + item.Hitbox.Width - 1, item.Hitbox.Y)) || IsPointInsideHitbox(new Vector2(item.Hitbox.X, item.Hitbox.Y + item.Hitbox.Height - 1)) || IsPointInsideHitbox(new Vector2(item.Hitbox.X + item.Hitbox.Width - 1, item.Hitbox.Y + item.Hitbox.Height - 1));
        }

        public void FillRemainingInputRecordWithIdle()
        {
            for(int i = inputRecordIndex; i < inputRecord.Length; i++)
            {
                inputRecord[i] = new InputState();
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameCamera camera)
        {
            if(UsedInLevel == false && IsActivePlayer == false)
            {
                return;
            }

            //sanity check
            if (currentSprite == null)
            {
                currentSprite = Sprites.SpritePlayer[Id];
            }

            Vector2 cameraTopLeft = new Vector2(camera.Position.X - camera.Width / 2, camera.Position.Y - camera.Height / 2);
            Vector2 drawPosition = new Vector2(Hitbox.X, Hitbox.Y) - cameraTopLeft;

            Rectangle sourceRect;

            //Draw player
            //if(Id == 0) //test animation with id 0 first
            if(false)
            {
                int frame = (int)currentAnimationFrame;

                sourceRect = new Rectangle(frame * 24, 0, 24, 36);    
            }
            else
            {
                sourceRect = new Rectangle(0, 0, 24, 36);
            }

            spriteBatch.Draw(currentSprite, drawPosition, sourceRect, Color.White);

            //Draw throw arrows
            if(throwingDirection == Direction.Right)
            {
                spriteBatch.Draw(Sprites.ThrowArrowH, drawPosition, Color.White);
            }
            else if (throwingDirection == Direction.Left)
            {
                spriteBatch.Draw(Sprites.ThrowArrowH, drawPosition, null, Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.FlipHorizontally, 0f);
            }
            else if (throwingDirection == Direction.Up)
            {
                spriteBatch.Draw(Sprites.ThrowArrowV, drawPosition, Color.White);
            }
            else if (throwingDirection == Direction.Down)
            {
                spriteBatch.Draw(Sprites.ThrowArrowV, drawPosition, null, Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.FlipVertically, 0f);
            }

            //Draw items that not all players have, e.g. pulse grenades and splits
            DrawSpecialItems(spriteBatch, camera);
            
            if (IsActivePlayer)
            {
                UiDrawer.DrawString(spriteBatch, "X: " + Hitbox.X, new Vector2(0, 0), Color.White);
                UiDrawer.DrawString(spriteBatch, "Y: " + Hitbox.Y, new Vector2(0, 30), Color.White);
                UiDrawer.DrawString(spriteBatch, "Grounded: " + IsOnGround, new Vector2(0, 120), Color.White);
            }
        }

        public virtual void DrawSpecialItems(SpriteBatch spriteBatch, GameCamera camera) {} //draw grenades, splits, etc.
    }
}
