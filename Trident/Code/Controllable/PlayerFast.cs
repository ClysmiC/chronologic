﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.DataObjects;
using Trident.Environment;
using Trident.Utility;
using Trident.Input;

namespace Trident.Controllable
{
    class PlayerFast : Player
    {
        public PlayerFast(Level level, Vector2 startingPosition)
            : base(level, startingPosition, 0)
        {
        }

        public override void SetVariablesBasedOnInputs(InputState input, out float xAcceleration, out float xMaxSpeed,
            out float xReverseAirAcceleration, out float xAirFriction, out float xGroundFriction, out float xMaxRunSpeed)
        {
            xAcceleration = (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerFastRunAcceleration : Constants.PlayerAcceleration;

            xMaxSpeed = (input.sprint.isDown) ? Constants.PlayerFastRunMaxSpeed : Constants.PlayerMaxSpeed;

            xReverseAirAcceleration = (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerFastRunReverseAirAcceleration : Constants.PlayerReverseAirAcceleration;

            xAirFriction = (throwingDirection != Direction.NoDirection && !IsOnGround) ? 0 : (input.sprint.isDown) ? Constants.PlayerRunAirFriction : Constants.PlayerAirFriction;

            xGroundFriction = (input.sprint.isDown) ? Constants.PlayerRunGroundFriction : Constants.PlayerGroundFriction;

            xMaxRunSpeed = Constants.PlayerFastRunMaxSpeed;
        }
    }
}
