﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trident.Utility;
using Trident.Input;

namespace Trident.Code.Menu
{
    public class MainMenu
    {
        private Rectangle windowRect;
        private const int NUM_ITEMS = 3;
        private const int ITEM_HEIGHT = 100;
        private const int ITEM_WIDTH = 500;
        private const int ITEM_V_MARGIN = 20;

        private MainMenuChoice selected;
		
		public MainMenuChoice ConfirmedSelection { get; private set; }

        public MainMenu()
        {
            windowRect = new Rectangle(0, 0, Resolution.VWidth, Resolution.VHeight);
            selected = MainMenuChoice.Play;
			ConfirmedSelection = MainMenuChoice.None;
        }

        public void Update(InputState input)
        {
            if(input.isPressed(input.down))
            {
                selected++;

                // 0 is NONE, so skip over it
                if((int)selected > NUM_ITEMS)
                {
                    selected = (MainMenuChoice) 1 ;
                }
            }
            else if (input.isPressed(input.up))
            {
                selected--;

                if (selected == MainMenuChoice.None)
                {
                    selected = (MainMenuChoice)NUM_ITEMS;
                }
            }

			if (input.isPressed(input.menuSelect))
			{
				ConfirmedSelection = selected;
			}
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprites.MainMenuBackground, windowRect, Color.White);

            float startY = Resolution.VHeight / 2 - ((ITEM_HEIGHT + ITEM_V_MARGIN) * (NUM_ITEMS / 2));

            for(int i = 0; i < NUM_ITEMS; i++)
            {
                float x = Resolution.VWidth / 2 - ITEM_WIDTH / 2;
                float y = startY + i * ITEM_HEIGHT + (i - 1) * ITEM_V_MARGIN;
                Rectangle source;

                if(i == (int)selected - 1)
                {

                    source = new Rectangle(0, (NUM_ITEMS + i) * ITEM_HEIGHT, ITEM_WIDTH, ITEM_HEIGHT);
                }
                else
                {
                    source = new Rectangle(0, i * ITEM_HEIGHT, ITEM_WIDTH, ITEM_HEIGHT);
                }
                
                spriteBatch.Draw(Sprites.MainMenuItems, new Vector2(x, y), source, Color.White);
            }

        }

        public enum MainMenuChoice
        {
            None = 0,
            Play,
            Level_Editor,
            Exit
        }
    }
}

