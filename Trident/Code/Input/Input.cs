using Trident.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using Trident.Environment;
using Trident.Code.Menu;
using System.Reflection;
using Trident.Input;

namespace Trident.Input
{
	public class DigitalInput
	{
		public bool isDown;
		public bool wasDown;

        public DigitalInput() { }

        public DigitalInput(bool isDown, bool wasDown)
        {
            this.isDown = isDown;
            this.wasDown = wasDown;
        }
	}

	public class InputState
	{
		public DigitalInput up = new DigitalInput();
		public DigitalInput down = new DigitalInput();
		public DigitalInput right = new DigitalInput();
		public DigitalInput left = new DigitalInput();
		public DigitalInput jump = new DigitalInput();
		public DigitalInput interact = new DigitalInput();
		public DigitalInput sprint = new DigitalInput();
		public DigitalInput special = new DigitalInput();
		public DigitalInput throwRight = new DigitalInput();
		public DigitalInput throwUp = new DigitalInput();
		public DigitalInput throwLeft = new DigitalInput();
		public DigitalInput throwDown = new DigitalInput();
		public DigitalInput idle = new DigitalInput();
		public DigitalInput endLife = new DigitalInput();
		public DigitalInput menuSelect = new DigitalInput();

        public InputState() { }

        public InputState(InputState copyTarget)
        {
			up = new DigitalInput(copyTarget.up.isDown, copyTarget.up.wasDown);
			down = new DigitalInput(copyTarget.down.isDown, copyTarget.down.wasDown);
			left = new DigitalInput(copyTarget.left.isDown, copyTarget.left.wasDown);
			right = new DigitalInput(copyTarget.right.isDown, copyTarget.right.wasDown);
			jump = new DigitalInput(copyTarget.jump.isDown, copyTarget.jump.wasDown);
			interact = new DigitalInput(copyTarget.interact.isDown, copyTarget.interact.wasDown);
			sprint = new DigitalInput(copyTarget.sprint.isDown, copyTarget.sprint.wasDown);
			special = new DigitalInput(copyTarget.special.isDown, copyTarget.special.wasDown);
			throwRight = new DigitalInput(copyTarget.throwRight.isDown, copyTarget.throwRight.wasDown);
			throwUp = new DigitalInput(copyTarget.throwUp.isDown, copyTarget.throwUp.wasDown);
			throwLeft = new DigitalInput(copyTarget.throwLeft.isDown, copyTarget.throwLeft.wasDown);
			throwDown = new DigitalInput(copyTarget.throwDown.isDown, copyTarget.throwDown.wasDown);
			idle = new DigitalInput(copyTarget.idle.isDown, copyTarget.idle.wasDown);
			endLife = new DigitalInput(copyTarget.endLife.isDown, copyTarget.endLife.wasDown);
			menuSelect = new DigitalInput(copyTarget.menuSelect.isDown, copyTarget.menuSelect.wasDown);
        }

		public bool isPressed(DigitalInput input)
		{
			return input.isDown && !input.wasDown;
		}

		public bool isReleased(DigitalInput input)
		{
			return !input.isDown && input.wasDown;
		}
			
		//TODO: instead of hard-coding key values, have them check against some sort of config file, which
		//can easily be edited via in-game menu.
		//TODO: once there is a config file, it needs to autogenerate a default one if the user deletes it :)
		public void UpdateInput()
		{
			// Note: these are built-in monogame classes
			KeyboardState kbState = Keyboard.GetState();
			GamePadState gpState = GamePad.GetState(PlayerIndex.One);

			const float stickThreshold = 0.5f;
			const float triggerThreshold = 0.12f;

			up.wasDown = up.isDown;
			down.wasDown = down.isDown;
			left.wasDown = left.isDown;
			right.wasDown = right.isDown;
			jump.wasDown = jump.isDown;
			interact.wasDown = interact.isDown;
			sprint.wasDown = sprint.isDown;
			special.wasDown = special.isDown;
			throwRight.wasDown = throwRight.isDown;
			throwUp.wasDown = throwUp.isDown;
			throwLeft.wasDown = throwLeft.isDown;
			throwDown.wasDown = throwDown.isDown;
			idle.wasDown = idle.isDown;
			endLife.wasDown = endLife.isDown;
			menuSelect.wasDown = menuSelect.isDown;

			up.isDown = kbState.IsKeyDown(Keys.Up) || gpState.ThumbSticks.Left.Y >= stickThreshold;
			down.isDown = kbState.IsKeyDown(Keys.Down) || gpState.ThumbSticks.Left.Y <= -stickThreshold;
			left.isDown = kbState.IsKeyDown(Keys.Left) || gpState.ThumbSticks.Left.X <= -stickThreshold;
			right.isDown = kbState.IsKeyDown(Keys.Right) || gpState.ThumbSticks.Left.X >= stickThreshold;
			jump.isDown = kbState.IsKeyDown(Keys.Space) || gpState.Buttons.A == ButtonState.Pressed;
			interact.isDown = kbState.IsKeyDown(Keys.OemSemicolon) || gpState.Buttons.X == ButtonState.Pressed;
			sprint.isDown = kbState.IsKeyDown(Keys.LeftShift) || gpState.Triggers.Right >= triggerThreshold;
			special.isDown = kbState.IsKeyDown(Keys.LeftControl) || gpState.Buttons.B == ButtonState.Pressed;
			throwRight.isDown = kbState.IsKeyDown(Keys.E) || gpState.ThumbSticks.Right.X >= stickThreshold;
			throwUp.isDown = kbState.IsKeyDown(Keys.OemComma) || gpState.ThumbSticks.Right.Y >= stickThreshold;
			throwLeft.isDown = kbState.IsKeyDown(Keys.A) || gpState.ThumbSticks.Right.X <= -stickThreshold;
			throwDown.isDown = kbState.IsKeyDown(Keys.O) || gpState.ThumbSticks.Right.Y <= -stickThreshold;
			idle.isDown = kbState.IsKeyDown(Keys.Tab) || gpState.Buttons.Y == ButtonState.Pressed;
			endLife.isDown = kbState.IsKeyDown(Keys.Escape) || gpState.Triggers.Left >= triggerThreshold;
			menuSelect.isDown = kbState.IsKeyDown(Keys.Enter) || gpState.Buttons.A == ButtonState.Pressed;
		}
	}
}
